/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_types.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/25 10:08:45 by mmichel           #+#    #+#             */
/*   Updated: 2016/02/10 17:22:15 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_TYPES_H
# define T_TYPES_H

# include "stddef.h"

# define BSHL(x,b) ((x) << (b))
# define BOR(a,b) ((a) | (b))
# define B(b) (BSHL(1, (b)))
# define LB(b) (long)(BSHL((long)1, (b)))
# define BANDEQ(b1, b2) ((b1 & b2) == b1)

# define SC(i) (sizeof(char) * (i))
# define SW(i) (sizeof(wchar_t) * (i))

# define FT_LL long long int
# define FT_UL long unsigned int
# define FT_ULL long long unsigned int
# define FT_UC unsigned char
# define FT_UI unsigned int
# define FT_SI short int
# define FT_US short unsigned int
# define FT_UNSIGNED unsigned

typedef long double		t_ld;

typedef FT_UC			t_uc;
typedef FT_US			t_us;
typedef FT_UI			t_ui;
typedef FT_UL			t_ul;
typedef FT_ULL			t_ull;
typedef FT_LL			t_ll;

typedef FT_UNSIGNED		t_u;

#endif
