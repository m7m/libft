/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_exit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/14 05:16:48 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/14 05:16:48 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_EXIT_H
# define T_EXIT_H

# define MAXLISTFREE 128

# include <stddef.h>
# include <stdlib.h>
# include <unistd.h>
# include "ft_debug.h"

typedef struct		s_sttexit
{
	void	*data;
	void	(*f)(void *);
}					t_sttexit;

void				ft_sttexit_cal(void);
void				ft_sttexit_del(t_sttexit *texit);
void				ft_sttexit_deldata(void *data);
void				ft_sttexit(void *texit);

void				ft_atexit(void (*f)(void));
void				ft_atexit_del(void (*f)(void));
void				ft_atexit_cal(void);

void				ft_vtexit(void *ptrfree);
void				ft_vtexit_del(void *ptr);
void				ft_vtexit_cal(void);

void				ft_fdtexit(int fd);
void				ft_fdtexit_del(int fd);
void				ft_fdtexit_cal(void);

void				ft_patexit(void (*f)(void *));

void				ft_exit(int status, char *msg);

#endif
