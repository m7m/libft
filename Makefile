#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/09/18 06:53:03 by mmichel           #+#    #+#              #
#    Updated: 2016/05/18 02:49:25 by mmichel          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

PREFIX	:= .
override INC = .
override LIBS = .
override SRC = src

SRCLIBC = $(shell find $(SRC) -type f | grep ".c$$" \
	| grep -vE "(/\.|main\.c|/\#)" )

UNAME := $(shell uname -s)
ifeq ($(UNAME),Darwin)
	ARCH := .osx.o
	ECHO := echo
else
	ARCH := .o
	ECHO := echo -e
endif
OBJ = $(SRCLIBC:.c=$(ARCH))

override LIBFT = $(LIBS)/libft.a

CC := gcc
LDLIBS := -L$(LIBS)
LDFLAGS := -lft
override CFLAGS += -Wall -Werror -Wextra -I$(INC)

EXTRACLEAN = $(shell find $(SRC) -type f | grep -E "(.gcov|.gcno|.gcda)$$")
EXTRACLEAN += $(shell find . -maxdepth 1 -type f \
	| grep -E "(.gcov|.gcno|.gcda)$$")
EXTRACLEAN_LOG = $(shell find $(LOGFILE) 2>&-)

NAME := $(LIBFT)

all: $(NAME)

ifndef VERBOSE
.SILENT: $(NAME) $(OBJ) (%$(ARCH).o)
endif

$(NAME): $(OBJ)
	@echo Create archive $(NAME)
	$(AR) cr $@ $^
	ranlib $@

clean:
	$(RM) -- $(OBJ)

fclean: clean
	$(RM) -- $(NAME)
ifneq (,$(findstring .g, $(EXTRACLEAN)))
	$(RM) -- $(EXTRACLEAN)
	$(RM) -r -- html
endif

re: fclean $(NAME)

%$(ARCH): %.c
	$(CC) -c $(CFLAGS) $< -o $@

.DEFAULT_GOAL := all
.PHONY: all re clean fclean
