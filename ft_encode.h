/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_encode.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:49:41 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:49:41 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_ENCODE_H
# define FT_ENCODE_H

# ifndef NONE
#  define NONE 0
# endif
# define UTF8 1

# define ENCODAGE UTF8

# if ENCODAGE == NONE
#  define FT_ENCODE(mstr, len) mstr
# elif ENCODAGE == UTF8
#  define FT_ENCODE(mstr, len) ft_encode_utf8(mstr, len)
# else
#  error Encode not supported
# endif

#endif
