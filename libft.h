/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 01:19:41 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/30 05:43:38 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# ifndef BUFF_SIZE
#  define BUFF_SIZE 2048
# endif

# include <string.h>
# include <stdlib.h>
# include <unistd.h>
# include <stdint.h>
# include "src/mem/ft_mem.h"
# include "src/tab/ft_tab.h"
# include "t_types.h"
# include "ft_encode.h"
# include "ft_debug.h"
# include "ft_os.h"
# include "ft_buff.h"
# include "t_exit.h"

# define MSG_OUT_OF_MEMORY "Out-of memory."
# define OUT_OF_MEMORY write(STDERR_FILENO, MSG_OUT_OF_MEMORY, 14)

int			ft_sqrt(int n);
int			ft_sqrt_arr_sup(int n);
double		ft_pow(double x, double y);
long double	ft_powl(long double x, long double y);
int			ft_isnan(char **nan, double n);

int			ft_intlen(int n);
size_t		ft_strlen(const char *s);
char		*ft_strcpy(char *dst, const char *src);
char		*ft_strncpy(char *dst, const char *src, size_t n);
char		*ft_stpcpy(char *dst, const char *src);
char		*ft_stpncpy(char *dst, const char *src, size_t n);
char		*ft_strcat(char *s1, const char *s2);
char		*ft_strncat(char *s1, const char *s2, size_t n);
size_t		ft_strlcat(char *dst, const char *src, size_t size);
char		*ft_strchr(const char *s, int c);
char		*ft_strrchr(const char *s, int c);
char		*ft_strchrnull(const char *s, int c);
char		*ft_strchrstrnull(const char *str, const char *chr);
char		*ft_strchrstr(const char *str, const char *chr);
char		*ft_strstr(const char *s1, const char *s2);
char		*ft_strnstr(const char *s1, const char *s2, size_t n);
int			ft_strcmp(const char *s1, const char *s2);
int			ft_strncmp(const char *s1, const char *s2, size_t n);
char		*ft_strsub(char const *s, unsigned int start, size_t len);
char		*ft_strjoin(char const *s1, char const *s2);
char		*ft_strtrim(char const *s);
char		**ft_strsplit(char const *s, char c);
void		ft_strsort(char **pstr, int len_pstr);
void		ft_strsortv2(char *list[]);

int			ft_atoi(const char *str);
long		ft_atol(const char *str);
double		ft_atod(const char *str);

wchar_t		*ft_encode_utf8(wchar_t *mstr, size_t len);
char		*ft_pwctoa(wchar_t *wc);
char		*ft_wctoa(wchar_t wc);
char		*ft_ctoa_base(char n, char *base);
char		*ft_uctoa_base(t_uc n, char *base);
char		*ft_stoa(short n);
char		*ft_ustoa(t_us n);
char		*ft_itoa(int n);
char		*ft_uitoa(unsigned int n);
char		*ft_ltoa(long n);
char		*ft_ultoa(t_ul n);
char		*ft_lltoa(t_ll n);
char		*ft_ulltoa(t_ull n);

char		*ft_itoa_base(int n, char *base);
char		*ft_uitoa_base(t_ui n, char *base);
char		*ft_ustoa_base(t_us n, char *base);
char		*ft_stoa_base(short n, char *base);
char		*ft_ultoa_base(t_ul n, char *base);
char		*ft_ltoa_base(long n, char *base);
char		*ft_lltoa_base(t_ll n, char *base);
char		*ft_ulltoa_base(t_ull n, char *base);
char		*ft_imtoa_base(intmax_t n, char *base);
char		*ft_uimtoa_base(uintmax_t n, char *base);
int			ft_itoa_s(int n, char *str);
t_ui		ft_uitoa_s(unsigned int n, char *str);
long		ft_ltoa_s(long n, char *str);
t_ul		ft_ultoa_s(unsigned long n, char *str);
size_t		ft_sizettoa_s(size_t n, char *str);
int			ft_itoa_base_s(int n, char *base, char *dst);
long		ft_ltoa_base_s(long n, char *base, char *dst);
int			ft_ultoa_base_s(t_ul n, char *base, char *dst);

int			ft_dto_str(double n, char *str, int expo, int preci);
int			ft_d_ret_expo(double n);
char		*ft_dtoa(double n, int precis);
int			ft_dtoa_expo(double *n);
char		*ft_dtoa_gg(double n, int precis);
char		*ft_dtoa_scien(double n, int precis);
char		*ft_dtoa_base16(double n, int precis);
char		*ft_hexdump(void *n, size_t size);
int			ft_isalpha(int c);
int			ft_isdigit(int c);
int			ft_isalnum(int c);
int			ft_isascii(int c);
int			ft_isprint(int c);
int			ft_isupper(int c);
int			ft_islower(int c);

size_t		ft_isutf8(wchar_t wc);
int			ft_strisdigit(char *s);
int			ft_toupper(int c);
int			ft_tolower(int c);

char		*ft_strnew(size_t size);
void		ft_strdel(char **as);
void		ft_strclr(char *s);
void		ft_striter(char *s, void (*f)(char *));
void		ft_striteri(char *s, void (*f)(unsigned int, char *));
char		*ft_strmap(char const *s, char (*f)(char));
char		*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int			ft_strequ(const char *s1, const char *s2);
int			ft_strnequ(char const *s1, char const *s2, size_t n);
char		*ft_strdup(const char *s1);
char		*ft_strdup_resize(const char *s1, size_t is1, size_t sp);
char		*ft_strresize(char *s, size_t n);
int			ft_strocu(const char *str);

void		ft_putchar(char c);
void		ft_putnchar(char c, int n);
void		ft_putnbr(int n);
void		ft_putnbrl(long n);
void		ft_putnbrul(t_ul n);
void		ft_putstr(char const *s);
void		ft_putstrf(char const *s, char fc);
void		*ft_puterror(char const *s);
void		ft_putstr_width(const char *str, int n);
void		ft_putnbr_width(int nb, int n);
void		ft_putstr_width_fd(const char *str, int n, int fd);
void		ft_putnbr_width_fd(int nb, int n, int fd);

void		ft_putendl(char const *s);
void		ft_putendl_fd(char const *s, int fd);
void		ft_putendl_size_fd(char const *s, size_t s_len, int fd);
void		ft_putendl_size(char const *s, size_t s_len);
void		ft_putchar_fd(char c, int fd);
void		ft_putstr_fd(char const *s, int fd);
void		ft_putnbr_fd(int n, int fd);
int			ft_printf(const char *format, ...);

void		ft_tabfree(char ***ptab);
void		ft_tabfree_size(char ***ptab, size_t tab_len);

#endif
