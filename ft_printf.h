/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 15:18:53 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/29 23:58:55 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <stdlib.h>
# include <wctype.h>
# include <wchar.h>
# include <stdint.h>
# include <stddef.h>
# include "libft.h"
# include "ft_printf_flags.h"
# include "ft_printf_struct.h"

# ifndef BUFF_SIZE
#  define BUFF_SIZE 1024
# endif

# define WARNERROR 0

# if MODLINUX == 1
#  define RET_FLAGS 0
#  define MSGNULL "(null)"
#  define MSGNULLPV "(nil)"
#  define FT_ATTTREE(prf, arg) ft_prf_atttree(prf, arg)
#  define FT_CONVERR(prf, arg) ft_prf_convtree_err(prf, arg)
# else
#  define RET_FLAGS 1
#  define MSGNULL "(null)"
#  define MSGNULLPV "0x0"
#  define FT_ATTTREE(prf, arg) ft_prf_attlen_tree(prf, arg)
#  define FT_CONVERR(prf, arg) ft_prf_convtree_errosx(prf, arg)
# endif

void		ft_prf_atexit(void *ptrfree);
void		ft_prf_exit(int status, char *msg);
void		ft_check_alloc(void *ptr);

int			ft_printf(const char *format, ...);
t_prfarg	*ft_lstprf_newadd(t_prfarg **lstprf);
void		ft_lstarg_addsort(t_prfarg **lstarg, t_prfarg *arg);
t_prfarg	*ft_lstarg_isexist(t_prfarg **lstarg, int ordre_ap);
t_prfarg	*ft_lstprf_delandnews(t_prfarg **lstarg, t_prfarg *arg);
t_prfarg	*ft_lstsort_arg(t_prf *prf);

int			ft_printf_parse(t_prf *prf);
void		ft_printf_arg(t_prf *prf);

t_prfarg	*ft_prf_parse_attchr(t_prf *prf);
int			ft_prf_attlen_tree(t_prf *prf, t_prfarg *arg);
int			ft_prf_atttree(t_prf *prf, t_prfarg *arg);

int			ft_prf_lentree(t_prf *prf, t_prfarg *arg);
void		ft_prf_lentree_hh(t_prf *prf, t_prfarg *arg);
void		ft_prf_lentree_ll(t_prf *prf, t_prfarg *arg);
int			ft_prf_convtree_errosx(t_prf *prf, t_prfarg *arg);
int			ft_prf_convtree_err(t_prf *prf, t_prfarg *arg);
int			ft_prf_convtree(t_prf *prf, t_prfarg *arg);
int			ft_prf_typetree(t_prf *prf, t_prfarg *arg);

int			ft_prf_attr_sppl(t_prf *prf, t_prfarg *arg);
int			ft_prf_attr_mo(t_prfarg *arg);
int			ft_prf_attr_co(t_prfarg *arg);
int			ft_prf_attr_re(t_prfarg *arg);
int			ft_prf_attr_pr(t_prf *prf, t_prfarg *arg);
int			ft_prf_attr_or(t_prf *prf, t_prfarg *arg);
int			ft_prf_attr_mu(t_prf *prf, t_prfarg *arg);
void		ft_prf_attr_mu_dol(t_prf *prf, t_prfarg *arg,
								t_prfarg **parg, int num_ap);
void		ft_prf_attr_mu_arg(t_prf *prf, t_prfarg *arg, t_prfarg **parg);

int			ft_prf_conv_di(t_prfarg *lstarg);
int			ft_prf_conv_ouxb(t_prfarg *lstarg);
int			ft_prf_conv_ee(t_prfarg *lstarg);
int			ft_prf_conv_ff(t_prfarg *lstarg);
int			ft_prf_conv_gg(t_prfarg *lstarg);
int			ft_prf_conv_aa(t_prfarg *lstarg);
int			ft_prf_conv_c(t_prfarg *lstarg);
int			ft_prf_conv_cc(t_prfarg *lstarg);
int			ft_prf_conv_s(t_prfarg *lstarg);
int			ft_prf_conv_p(t_prfarg *lstarg);
int			ft_prf_conv_n(t_prfarg *lstarg);
int			ft_prf_conv_fd(t_prfarg *lstarg);

void		ft_apl_attr_mo(t_prfarg *arg);
void		ft_apl_attr_co(t_prfarg *arg);
void		ft_apl_attr_re(t_prfarg *arg);
void		ft_apl_attr_pr(t_prfarg *arg);
void		ft_apl_attr_or(t_prfarg *arg);
void		ft_apl_attr_pl(t_prfarg *arg);
void		ft_apl_attr_sp(t_prfarg *arg);
void		ft_apl_attr_width(t_prfarg *arg);

void		ft_type_char(t_prf *prf, t_prfarg *arg);
void		ft_type_charshort(t_prf *prf, t_prfarg *arg);
void		ft_type_wchar_t(t_prf *prf, t_prfarg *arg);
void		ft_type_short(t_prf *prf, t_prfarg *arg);
void		ft_type_int(t_prf *prf, t_prfarg *arg);
void		ft_type_long(t_prf *prf, t_prfarg *arg);
void		ft_type_longlong(t_prf *prf, t_prfarg *arg);
void		ft_type_double(t_prf *prf, t_prfarg *arg);
void		ft_type_double_fmt(t_prfarg *arg);
void		ft_type_doubledouble(t_prf *prf, t_prfarg *arg);
void		ft_type_intmax_t(t_prf *prf, t_prfarg *arg);
void		ft_type_size_t(t_prf *prf, t_prfarg *arg);
void		ft_type_ptrdiff_t(t_prf *prf, t_prfarg *arg);
void		ft_type_fd(t_prf *prf, t_prfarg *arg);
int			ft_ptype(t_prfarg *arg, t_prfarg *pc_arg, t_ui type);
int			ft_ptype_int(t_prfarg *parg);

void		ft_type_uchar(t_prf *prf, t_prfarg *arg);
void		ft_type_ushort(t_prf *prf, t_prfarg *arg);
void		ft_type_uint(t_prf *prf, t_prfarg *arg);
void		ft_type_ulong(t_prf *prf, t_prfarg *arg);
void		ft_type_ulonglong(t_prf *prf, t_prfarg *arg);
void		ft_type_uintmax_t(t_prf *prf, t_prfarg *arg);
void		ft_type_usize_t(t_prf *prf, t_prfarg *arg);
void		ft_type_uptrdiff_t(t_prf *prf, t_prfarg *arg);

void		ft_type_pchar(t_prf *prf, t_prfarg *arg);
void		ft_type_pwchar_t(t_prf *prf, t_prfarg *arg);
void		ft_type_pint(t_prf *prf, t_prfarg *arg);
void		ft_type_pvoid(t_prf *prf, t_prfarg *arg);
void		ft_type_px(t_prf *prf, t_prfarg *arg);

void		ft_apl_attr(t_prf *prf);

#endif
