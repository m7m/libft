/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_debug.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:49:14 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:49:28 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_DEBUG_H
# define FT_DEBUG_H

# include "libft.h"
# include <unistd.h>

# include <stdio.h>
# include <time.h>

# ifndef FILE_DEBUG_LOG
#  define FILE_DEBUG_LOG "./debug.log"
# endif

void	ft_debug_common(const char *fonc, int li, const char *c, const char *n);
void	ft_debugstr(const char *s);
void	ft_debugnbr(int n);
void	ft_debugnbrl(long n);
void	ft_debugnbrul(t_ul n, char *base);
void	ft_debugc(const char c);
void	ft_debugptr(void *ptr);

int		ft_retfd(void);
void	ft_debug_common_fd(const char *fonc, int li,
						const char *c, const char *n);
void	ft_debugstr_fd(const char *s);
void	ft_debugnbr_fd(int n);
void	ft_debugnbrl_fd(long n);
void	ft_debugnbrul_fd(t_ul n, char *base);
void	ft_debugc_fd(const char c);
void	ft_debugptr_fd(void *ptr);

FILE	*ft_retstream(void);

# ifdef DEBUG
#  undef DEBUG
# endif
# ifndef DEBUG_LVL
#  define DEBUG_LVL 0
# endif

# if DEBUG_LVL == 1
#  define DFD ft_retfd()
#  define DPID getpid()
#  define DFL __FUNCTION__, __LINE__
#  define DFMT_00P0 "%P" "%-8d" "%-30.30s" ": " "%-4d\n"
#  define DFMT_01P0 "%P%-8d" "%-30.30s" ": " "%-4d" ": " "%15.12p " "%20.20s = "
#  define DEND ft_fflush(DFD)
#  define DFTTF_1P0(x, xx)     ft_printf(DFMT_01P0, DFD, DPID, DFL, x, xx);DEND
#  define DFTTF_2P1(t, x)  ft_printf("%1P%-1" t, DFD, x)
#  define DCOMMON(t, x, xx)    DFTTF_1P0(x, xx);DFTTF_2P1(t, x);

#  define DEBUG            ft_printf(DFMT_00P0, DFD, DPID, DFL);ft_fflush(DFD)
#  define DEBUGC(x)        DCOMMON("c\n", x, #x)
#  define DEBUGWC(x)       DCOMMON("lc\n", x, #x)
#  define DEBUGSTR(x)      DCOMMON("s\n", x, #x)
#  define DEBUGWCS(x)      DCOMMON("ls\n", x, #x)
#  define DEBUGPTR(x)      DCOMMON("%\n", x, #x)
#  define DEBUGNBR(x)      DCOMMON("d\n", x, #x)
#  define DEBUGNBRU(x)     DCOMMON("u\n", x, #x)
#  define DEBUGNBRUC(x)    DCOMMON("hhu\n", x, #x)
#  define DEBUGNBRL(x)     DCOMMON("ld\n", x, #x)
#  define DEBUGNBRUL(x)    DCOMMON("lu\n", x, #x)
#  define DEBUGNBRULB2(x)  DCOMMON("B\n", x, #x)
#  define DEBUGNBRULB16(x) DCOMMON("lx\n", x, #x)
#  define COM(x) DEBUG

# elif DEBUG_LVL == 2
#  define DEBUGIDEM __FUNCTION__, __LINE__,
#  define DEBUG ft_debug_common_fd(DEBUGIDEM "\n", NULL)
#  define DEBUGD ft_putstr(__FUNCTION__);ft_putnbr(__LINE__);ft_putchar('\n')
#  define DEBUGCOM(x) ft_debug_common_fd(DEBUGIDEM ":", #x)
#  define DEBUGSTR(x) DEBUGCOM(x);ft_debugstr_fd(x)
#  define DEBUGWCS(x)
#  define DEBUGC(x) DEBUGCOM(x);ft_debugc_fd(x)
#  define DEBUGWC(x)
#  define DEBUGPTR(x) DEBUGCOM(x);ft_debugptr_fd(x)
#  define DEBUGNBR(x) DEBUGCOM(x);ft_debugnbr_fd(x)
#  define DEBUGNBRL(x) DEBUGCOM(x);ft_debugnbrl_fd(x)
#  define DEBUGNBRU(x)
#  define DEBUGNBRUC(x)
#  define DEBUGNBRUL(x) DEBUGCOM(x);ft_debugnbrul_fd(x, "0123456789")
#  define DEBUGNBRULB2(x) DEBUGCOM(x);ft_debugnbrul_fd(x, "01")
#  define DEBUGNBRULB16(x) DEBUGCOM(x);ft_debugnbrul_fd(x, "0123456789ABCEDF")
#  define COM(x) DEBUG

# elif DEBUG_LVL == 3
#  define DEBUG ft_putstr(__FUNCTION__);ft_putnbr(__LINE__);ft_putchar('\n')
#  define DEBUGCOM(x) DEBUG
#  define DEBUGSTR(x) DEBUG
#  define DEBUGC(x) DEBUG
#  define DEBUGPTR(x) DEBUG
#  define DEBUGNBR(x) DEBUG
#  define DEBUGNBRL(x) DEBUG
#  define DEBUGNBRUL(x) DEBUG
#  define DEBUGNBRULB2(x) DEBUG
#  define DEBUGNBRULB16(x) DEBUG
#  define COM(x) DEBUG
#  define DEBUGWC(x) DEBUG
#  define DEBUGWCS(x) DEBUG
#  define DEBUGNBRUC(x) DEBUG

# elif DEBUG_LVL == 4
#  define DEBUGIDEM __FUNCTION__, __LINE__,
#  define DEBUG ft_debug_common(DEBUGIDEM "\n", NULL);
#  define DEBUGCOM(x) ft_debug_common(DEBUGIDEM ":", #x)
#  define DEBUGSTR(x) DEBUGCOM(x);ft_debugstr(x)
#  define DEBUGNBR(x) DEBUGCOM(x);ft_debugnbr(x)
#  define DEBUGNBRL(x) DEBUGCOM(x);ft_debugnbrl(x)
#  define DEBUGNBRUL(x) DEBUGCOM(x);ft_debugnbrul(x, "0123456789")
#  define DEBUGNBRULB2(x) DEBUGCOM(x);ft_debugnbrul(x, "01")
#  define DEBUGNBRULB16(x) DEBUGCOM(x);ft_debugnbrul(x, "0123456789ABCEDF")
#  define DEBUGC(x) DEBUGCOM(x);ft_debugc(x)
#  define DEBUGPTR(x) DEBUGCOM(x);ft_debugptr(x)
#  define COM(x) DEBUG
#  define DEBUGWC(x)
#  define DEBUGWCS(x)
#  define DEBUGNBRUC(x)

# elif DEBUG_LVL == 5

#  define DFD ft_retstream() ? ft_retstream() : stderr
#  define DPID getpid()
#  define DFL __FUNCTION__, __LINE__
#  define DFMT_01P0 "%P%-8d" "%-30.30s" ": " "%-4d" ": " "%15.12p " "%20.20s = "
#  define DEND

#  define DFMT_00P0 "% 6zd" ": " "%-8d" "%-30.30s" ": " "%-4d"

#  define FPRINT_01 DFD, DFMT_00P0 ": " "%15p " "%20.20s = " "%-1"
#  define FPRINT_02 clock(), getpid(), __FUNCTION__, __LINE__
#  define DFTTF_1P0(t, px, x, xx) fprintf(FPRINT_01 t , FPRINT_02, px, xx, x)
#  define DCOMMON(t, px, x, xx)    DFTTF_1P0(t, px, x, xx);DEND

#  define DEBUG            fprintf(DFD, DFMT_00P0 "\n", clock(), DPID, DFL);DEND

#  define DEBUGC(x)        DCOMMON("c\n", (void *)&x, x, #x)
#  define DEBUGWC(x)       DCOMMON("lc\n", (void *)&x, x, #x)
#  define DEBUGSTR(x)      DCOMMON("s\n", (void *)x, (char *)x, #x)
#  define DEBUGWCS(x)      DCOMMON("ls\n", (void *)x, x, #x)
#  define DEBUGPTR(x)      DCOMMON("p\n", (void *)x, x, #x)

#  define DEBUGNBR(x)      DCOMMON("d\n", NULL, (int)(x), #x)
#  define DEBUGNBRU(x)     DCOMMON("u\n", NULL, x, #x)
#  define DEBUGNBRUC(x)    DCOMMON("hhu\n", (void *)&x, x, #x)
#  define DEBUGNBRL(x)     DCOMMON("ld\n", (void *)x, (long)x, #x)
#  define DEBUGNBRUL(x)    DCOMMON("lu\n", NULL, (unsigned long)x, #x)
#  define DEBUGNBRULB2(x)  DCOMMON("B\n", (void *)x, x, #x)
#  define DEBUGNBRULB16(x) DCOMMON("lx\n", (void *)x, x, #x)
#  define COM(x) DEBUG

#  define DEBUGNBRUL_COND(x) if (x) {DEBUGNBRUL(x);}

# else
#  define DEBUG
#  define DEBUGC(x)
#  define DEBUGWC(x)
#  define DEBUGSTR(x)
#  define DEBUGWCS(x)
#  define DEBUGPTR(x)
#  define DEBUGNBR(x)
#  define DEBUGNBRU(x)
#  define DEBUGNBRUC(x)
#  define DEBUGNBRL(x)
#  define DEBUGNBRUL(x)
#  define DEBUGNBRULB2(x)
#  define DEBUGNBRULB16(x)
#  define COM(x)
# endif

# if DEBUG_LVL != 0
#  define DOK ft_exit(EXIT_FAILURE, "test ok\n");
#  define DKO ft_exit(EXIT_FAILURE, "test ko\n");
#  define DSTOP DEBUG;(void)read(STDIN_FILENO, 0, 1);
# else
#  define DOK
#  define DKO
#  define DSTOP
# endif

#endif
