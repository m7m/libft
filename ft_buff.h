/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_buff.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/13 20:44:43 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/13 20:44:43 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_BUFF_H
# define FT_BUFF_H

# include "libft.h"

# ifndef MAXFD
#  define MAXFD 65536
# endif

# ifndef BUFF_SIZE
#  define BUFF_SIZE 256
# endif

void	ft_fflush_all(void);
void	ft_fflush(int fd);
int		ft_buff(int fd, void *data, size_t data_len);

#endif
