/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchrstr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 08:10:43 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/03 08:10:43 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Recherche dans str l'un des caracterre de chr
*/

char
	*ft_strchrstr(const char *str, const char *chr)
{
	const char	*c;

	while (*str)
	{
		c = chr;
		while (*c)
		{
			if (*c == *str)
				return ((char *)str);
			++c;
		}
		++str;
	}
	return (NULL);
}

char
	*ft_strchrstrnull(const char *str, const char *chr)
{
	const char	*c;

	while (*str)
	{
		c = chr;
		while (*c)
		{
			if (*c == *str)
				return ((char *)str);
			++c;
		}
		++str;
	}
	return ((char *)str);
}
