/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strresize.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 08:45:32 by mmichel           #+#    #+#             */
/*   Updated: 2015/11/30 13:47:19 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Reallou de plus n et libere l'ancienne
** Retourne la copie avec plus n
*/

#include "libft.h"

char	*ft_strresize(char *s, size_t n)
{
	char	*res;
	size_t	len;

	len = ft_strlen((char *)s);
	res = (char *)malloc(sizeof(s) * (len + n + 1));
	if (res == NULL)
		return (NULL);
	ft_memcpy(res, s, len + sizeof(char));
	res[len + n] = 0;
	free(s);
	return (res);
}
