/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hexdump.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:15:44 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:15:45 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
** Convertie (n)2 en base 16
*/

char		*ft_hexdump(void *n, size_t size)
{
	size_t	i;
	size_t	istr;
	char	*base16;
	char	*str;

	str = NULL;
	if (n && (str = (char *)malloc(sizeof(char) * (size * 2 + 1))))
	{
		base16 = "0123456789ABCDEF";
		i = 0;
		istr = size;
		while (i < size)
		{
			str[--istr * 2] = base16[(((char *)n)[i] & 0xF0) >> 4];
			str[istr * 2 + 1] = base16[((char *)n)[i] & 0xF];
			i += 1;
		}
		str[size * 2] = 0;
	}
	return (str);
}
