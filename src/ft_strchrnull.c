/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchrnull.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:08:13 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:08:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

/*
** La fonction strchrnul() est comme strchr()
** excepté que si c n'est pas trouvé dans s,
** elle renvoie un pointeur sur l'octet nul à la fin de s,
** plutôt que NULL.
*/

char	*ft_strchrnull(const char *s, int c)
{
	while (*s)
		if (*s == c)
			return ((char *)s);
		else
			++s;
	return ((char *)s);
}
