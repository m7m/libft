/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/18 01:24:15 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/18 01:49:23 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_exit(int status, char *msg)
{
	DEBUG;
	if (msg)
	{
		if (status == EXIT_FAILURE)
			ft_putstr_fd(msg, STDERR_FILENO);
		else
			ft_putstr_fd(msg, STDOUT_FILENO);
	}
	DEBUGSTR("at");
	ft_atexit_cal();
	DEBUGSTR("st");
	ft_sttexit_cal();
	DEBUGSTR("vt");
	ft_vtexit_cal();
	DEBUGSTR("fd");
	ft_fdtexit_cal();
	ft_fflush_all();
	exit(status);
}
