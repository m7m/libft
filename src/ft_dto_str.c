/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dto_str.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:13:01 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:13:05 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_dto_str_exp_pos(double *n, char *str, int i, double arround)
{
	double	pow;
	int		j;
	int		errarround;

	j = -1;
	pow = ft_pow(10., (double)i);
	errarround = (int)((*n + arround) / pow);
	DEBUGNBR(j);
	if (errarround)
		str[++j] = 49;
	DEBUGNBR(j);
	while (i)
	{
		pow = ft_pow(10., (double)--i);
		errarround = (int)((*n + arround) / pow);
		if (errarround > 9)
			str[++j] = (int)((*n + arround) / pow) + 38;
		else
			str[++j] = (int)((*n + arround) / pow) + 48;
		*n -= ((int)(*n / pow) * pow);
	}
	DEBUGSTR(str);
	return (j);
}

static int	ft_dto_str_exp_neg(double n, char *str, int j, int preci)
{
	double	arround;
	int		i;
	int		errarround;

	i = 0;
	while (++i <= preci)
	{
		arround = 5 / ft_pow(10., preci - i);
		n *= 10;
		errarround = (int)(n + arround);
		if (errarround > 9)
			str[++j] = errarround + 38;
		else
			str[++j] = errarround + 48;
		n = (n - (int)n);
	}
	return (j);
}

/*
** Convert double to string
** Ne gére pas la négation de n
** Retourne la longeur de str
*/

int			ft_dto_str(double n, char *str, int expo, int preci)
{
	int		j;

	DEBUGNBR(expo);
	DEBUGNBR(preci);
	if (n < 0)
		n = -n;
	if (expo < 0)
		expo = 0;
	j = ft_dto_str_exp_pos(&n, str, expo + 1,
							5 / ft_pow(10., (double)(preci + 1)));
	if (preci)
		str[++j] = '.';
	else
		++j;
	j = ft_dto_str_exp_neg(n, str, j, preci + 1);
	str[j] = 0;
	DEBUGSTR(str);
	DEBUGNBR(j);
	return (j);
}
