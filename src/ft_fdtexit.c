/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fdtexit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/13 22:36:36 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/13 22:36:36 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Sauvegarde fd dans une liste
** si ptrfree et null alors free la liste
*/

static int
	*ft_fdtexit__static(void)
{
	static int	iall[MAXLISTFREE + 1] = {-5};
	int			i;

	if (iall[0] == -5)
	{
		i = 0;
		while (i < MAXLISTFREE)
		{
			iall[i] = -1;
			++i;
		}
	}
	return (iall);
}

void
	ft_fdtexit_cal(void)
{
	int	i;
	int	*iall;

	DEBUG;
	i = 0;
	iall = ft_fdtexit__static();
	while (i < MAXLISTFREE && iall[i] > -1)
	{
		close(iall[i]);
		iall[i] = -1;
		++i;
	}
}

void
	ft_fdtexit_del(int fd)
{
	int	i;
	int	*iall;

	DEBUG;
	if (fd > -1)
	{
		i = 0;
		iall = ft_fdtexit__static();
		while (i < MAXLISTFREE && iall[i] > -1 && iall[i] != fd)
			++i;
		if (i >= MAXLISTFREE)
			return ;
		if (iall[i] > -1)
			iall[i] = -1;
		while (i + 1 < MAXLISTFREE && iall[i + 1] > -1)
		{
			iall[i] = iall[i + 1];
			iall[i + 1] = -1;
			++i;
		}
	}
}

void
	ft_fdtexit(int fd)
{
	int	i;
	int	*iall;

	DEBUG;
	if (fd > -1)
	{
		i = 0;
		iall = ft_fdtexit__static();
		while (i < MAXLISTFREE && iall[i] > -1)
			++i;
		if (i >= MAXLISTFREE)
			ft_exit(EXIT_FAILURE, "ft_fdtexit: overflow MAXLISTFREE\n");
		iall[i] = fd;
	}
}
