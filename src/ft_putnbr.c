/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 01:32:05 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/02 13:38:50 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr(int n)
{
	if (n > 9)
	{
		ft_putnbr(n / 10);
		ft_putchar(n - (n / 10 * 10) + 48);
	}
	else if (n < 0)
	{
		ft_putchar('-');
		if (n < -9)
			ft_putnbr((0 - (unsigned int)n) / 10);
		ft_putchar(((n - (n / 10 * 10)) * -1) + 48);
	}
	else
		ft_putchar(n + 48);
}

void	ft_putnbrl(long n)
{
	if (n > 9)
	{
		ft_putnbrl(n / 10);
		ft_putchar(n - (n / 10 * 10) + 48);
	}
	else if (n < 0)
	{
		ft_putchar('-');
		if (n < -9)
			ft_putnbrl((0 - (unsigned long)n) / 10);
		ft_putchar(((n - (n / 10 * 10)) * -1) + 48);
	}
	else
		ft_putchar(n + 48);
}

void	ft_putnbrul(t_ul n)
{
	if (n > 9)
	{
		ft_putnbrul(n / 10);
		ft_putchar(n - (n / 10 * 10) + 48);
	}
	else
		ft_putchar(n + 48);
}
