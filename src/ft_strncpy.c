/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 12:52:57 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/23 01:43:29 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char	*ft_strncpy(char *dst, const char *src, size_t n)
{
	int i;

	i = -1;
	++n;
	while (--n)
		if (src[0])
		{
			dst[++i] = src[0];
			++src;
		}
		else
			dst[++i] = 0;
	return (dst);
}
