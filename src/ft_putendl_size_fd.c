/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_size_fd.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/14 02:50:35 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/14 02:50:35 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putendl_size_fd(char const *s, size_t s_len, int fd)
{
	write(fd, s, s_len);
	write(fd, "\n", 1);
}
