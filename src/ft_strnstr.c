/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 02:44:09 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/05 01:13:24 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** locate a substring in a string
*/

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t	i;
	size_t	ii;
	size_t	lens2;
	size_t	ms1;

	ms1 = 0;
	ii = 0;
	lens2 = ft_strlen(s2);
	i = 0;
	while (s1[ii] && s2[i] && ii < n)
	{
		if (s1[ii] == s2[i])
			i++;
		else
		{
			ii = ms1++;
			i = 0;
		}
		ii++;
	}
	if (i == lens2)
		return ((char *)s1 + ii - i);
	return (NULL);
}
