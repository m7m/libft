/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 05:49:11 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/08 06:51:16 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_atoi(const char *str)
{
	int i;
	int m;

	i = 0;
	m = 1;
	while (*str > 0 && *str <= 32)
		str = str + 1;
	if (*str == '-')
		m = *str++ - 46;
	else if (*str == '+')
		m = 44 - *str++;
	while (*str)
	{
		if (*str > 47 && *str < 58)
			i = i * 10 + *str - 48;
		else if (i || m)
			return (i * m);
		str++;
	}
	return (i * m);
}
