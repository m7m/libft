/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabfree.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/11 06:37:25 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/11 06:37:25 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_debug.h"

/*
** Free tab[][]
*/

void
	ft_tabfree_cont(char **tab)
{
	while (*tab)
	{
		free(*tab);
		*tab = NULL;
		++tab;
	}
}

void
	ft_tabfree_size(char ***ptab, size_t tab_len)
{
	char	**tab;

	tab = *ptab;
	while (tab_len)
	{
		--tab_len;
		DEBUGPTR(tab[tab_len]);
		free(tab[tab_len]);
	}
	free(tab);
	*ptab = NULL;
}

void
	ft_tabfree(char ***ptab)
{
	ft_tabfree_cont(*ptab);
	free(*ptab);
	*ptab = NULL;
}
