/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabadd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/19 17:59:23 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/19 17:59:23 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char
	**ft_tabins(char *tab[], char *add)
{
	size_t	len;

	DEBUG;
	len = 0;
	while (tab[len])
		++len;
	++len;
	while (len)
	{
		tab[len] = tab[len - 1];
		--len;
	}
	*tab = add;
	return (tab);
}

/*
** Trie en ajoutant
** le tableau doit être suffisament grand pour l'ajout
** retourne la position dans le tableau
*/

char
	**ft_tabsort_add(char *tab[], char *add)
{
	char	**ptab;

	ptab = tab;
	DEBUG;
	while (*ptab)
	{
		if (ft_strcmp(*ptab, add) >= 0)
		{
			ft_tabins(ptab, add);
			return (ptab);
		}
		++ptab;
	}
	*ptab = add;
	ptab[1] = NULL;
	return (ptab);
}

char
	**ft_tabsort_add_igndbl(char *tab[], char *add)
{
	int		ret;
	char	**ptab;

	ptab = tab;
	DEBUG;
	while (*ptab)
	{
		if ((ret = ft_strcmp(*ptab, add)) > 0)
		{
			ft_tabins(ptab, add);
			return (ptab);
		}
		else if (!ret)
			return (NULL);
		++ptab;
	}
	*ptab = add;
	ptab[1] = NULL;
	return (ptab);
}

char
	**ft_tabresize(char **tab, int plus_len)
{
	int		len;
	char	**tabtmp;

	len = ft_tablen(tab);
	if (!(tabtmp = (char **)malloc(sizeof(char *) * (len + plus_len + 1))))
		return (NULL);
	tabtmp[len] = NULL;
	while (len)
	{
		--len;
		DEBUGSTR(tab[len]);
		tabtmp[len] = tab[len];
	}
	return (tabtmp);
}
