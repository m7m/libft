/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_taballoc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 07:03:52 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 07:03:52 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char
	**ft_taballocadd_size(char **tab, size_t len, char *add)
{
	char	**tmptab;
	size_t	i;

	i = 0;
	if (!(tmptab = (char **)malloc(sizeof(char *) * (len + 2))))
		ft_exit(EXIT_FAILURE, MSG_OUT_OF_MEMORY);
	tmptab[len] = add;
	tmptab[len + 1] = 0;
	while (i < len)
	{
		tmptab[i] = tab[i];
		++i;
	}
	free(tab);
	return (tmptab);
}

char
	**ft_taballocadd(char **tab, char *add)
{
	size_t	len;

	len = 0;
	while (tab[len])
		++len;
	return (ft_taballocadd_size(tab, len, add));
}

char
	**ft_taballocins(char **tab, char *add)
{
	char	**tmptab;
	size_t	i;
	size_t	len;

	len = 0;
	while (tab[len])
		++len;
	if (!(tmptab = (char **)malloc(sizeof(char *) * (len + 2))))
		ft_exit(EXIT_FAILURE, MSG_OUT_OF_MEMORY);
	*tmptab = add;
	++len;
	tmptab[len] = 0;
	i = 1;
	while (i < len)
	{
		DEBUGSTR(tab[i]);
		tmptab[i] = tab[i - 1];
		++i;
	}
	return (tmptab);
}

char
	**ft_taballocjoint(char **dsttab, size_t dsttablen,
					char **srctab, size_t srctablen)
{
	char	**tmptab;
	size_t	i;
	size_t	len;

	len = dsttablen + srctablen;
	if (!(tmptab = (char **)malloc(sizeof(char *) * (len + 1))))
		ft_exit(EXIT_FAILURE, MSG_OUT_OF_MEMORY);
	tmptab[len] = NULL;
	i = 0;
	while (i < dsttablen)
	{
		DEBUGSTR(dsttab[i]);
		tmptab[i] = dsttab[i];
		++i;
	}
	while (i < len)
	{
		DEBUGSTR(srctab[i - dsttablen]);
		tmptab[i] = srctab[i - dsttablen];
		++i;
	}
	return (tmptab);
}
