/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tab.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/03 13:24:42 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/03 13:24:42 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_TAB_H
# define FT_TAB_H

# include <stddef.h>

int		ft_tablen(char **tab);
size_t	ft_tabsize(char **tab);
char	**ft_tabdup(char **tab);
char	**ft_taballocadd(char **tab, char *add);
char	**ft_taballocins(char **tab, char *add);
char	**ft_taballocadd_size(char **tab, size_t len, char *add);
char	**ft_taballocjoint(char **dsttab, size_t dsttablen,
						char **srctab, size_t srctablen);
char	**ft_tabsort_add(char *tab[], char *add);
char	**ft_tabsort_add_igndbl(char *tab[], char *add);
char	**ft_tabresize(char **tab, int plus_len);
void	ft_tabdel(char **tab, int index);

int		*ft_tabsort_addint(size_t len, int *tab, int add);
int		*ft_tabins_int(size_t len, int *tab, int add);
int		ft_tabchr_int(size_t len, int *tab, int c);

#endif
