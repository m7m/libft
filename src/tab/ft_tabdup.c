/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/21 02:55:47 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/21 02:55:47 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stddef.h>
#include "libft.h"
#include "ft_debug.h"

char
	**ft_tabdup(char **tab)
{
	size_t	j;
	size_t	len;
	char	**ntab;
	char	*ptab;

	DEBUG;
	if ((ntab = (char **)malloc((ft_tabsize(tab) + 1))))
	{
		ptab = (char *)(ntab + ft_tablen(tab) + 1);
		j = 0;
		len = 0;
		while (tab[j])
		{
			ntab[j] = ptab + len;
			DEBUGNBR(j);
			DEBUGSTR(tab[j]);
			ft_strcpy(ntab[j], tab[j]);
			DEBUGSTR(ntab[j]);
			len += ft_strlen(tab[j]) + 1;
			++j;
		}
		ntab[j] = NULL;
	}
	return (ntab);
}
