/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabchr_int.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/03 13:48:57 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/03 13:48:57 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Cherche c dans tab si non trouver retourne -1
** sinon retourn l'index correspondant
*/

int
	ft_tabchr_int(size_t len, int *tab, int c)
{
	size_t	i;

	i = 0;
	while (i < len)
	{
		if (tab[i] == c)
			return (i);
		++i;
	}
	return (-1);
}
