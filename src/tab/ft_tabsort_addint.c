/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabsort_addint.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/03 13:38:46 by mmichel           #+#    #+#             */
/*   Updated: 2016/12/03 13:38:46 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

int
	*ft_tabins_int(size_t len, int *tab, int add)
{
	while (len)
	{
		tab[len] = tab[len - 1];
		--len;
	}
	*tab = add;
	return (tab);
}

/*
** Ajoute dans l'ordre croissand
*/

int
	*ft_tabsort_addint(size_t len, int *tab, int add)
{
	size_t	i;

	i = 0;
	while (i < len)
	{
		if (tab[i] >= add)
		{
			ft_tabins_int(len - i, tab + i, add);
			return (tab + i);
		}
		++i;
	}
	tab[i] = add;
	return (tab + i);
}
