/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wctoa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:08:25 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:45:41 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_pwctoa(wchar_t *wc)
{
	int		i;
	int		len;
	char	*str;
	char	*tmp;

	i = -1;
	if (wc)
		while (wc[++i])
			;
	len = i * sizeof(wchar_t);
	str = NULL;
	if (wc && (wc = FT_ENCODE(wc, i))
		&& (str = ft_strnew(sizeof(char) * (len + 1))))
	{
		tmp = (char *)wc;
		i = -1;
		while ((char *)tmp - (char *)wc < len)
			if (*tmp)
				str[++i] = *tmp++;
			else
				++tmp;
	}
	if (ENCODAGE)
		free(wc);
	return (str);
}

char	*ft_wctoa(wchar_t wc)
{
	int		len;
	int		i;
	t_uc	*str;
	t_uc	*tmp;
	t_uc	*tmpstr;

	len = sizeof(wchar_t);
	if (ENCODAGE)
		return ((char *)FT_ENCODE(&wc, 1));
	if (!(str = (t_uc *)ft_strnew(sizeof(char) * (sizeof(wchar_t) + 1))))
		return (NULL);
	tmpstr = str;
	tmp = (unsigned char *)&wc;
	i = -1;
	while (++i < len)
		if (tmp[i])
			*tmpstr++ = tmp[i];
	return ((char *)str);
}
