/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 07:38:12 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/01 12:30:36 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Alloue (avec malloc(3)) et retourne la copie "fraiche" d'un troncon
** de la chaine de caracteres passee en parametre.
** Le troncon commence a index start et a pour longueur len.
** Si start et len ne designent pas un troncon de chaine valide,
** le comportement est indetermine. Si allocation echoue,
** la fonction renvoie NULL.
*/

#include "libft.h"

/*
** Substitut dans 's' à partir de 'start' de taille 'len'
*/

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char		*tronc;

	tronc = (char *)ft_memalloc(sizeof(char) * (len + 1));
	if (tronc)
	{
		ft_strncpy(tronc, s + start, len);
		tronc[len] = 0;
	}
	return (tronc);
}
