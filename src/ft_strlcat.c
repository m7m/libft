/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 20:46:07 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/02 13:30:27 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst,
							const char *src,
							size_t size)
{
	char	*tmp;
	size_t	lendst;
	size_t	lensrc;

	lendst = ft_strlen(dst);
	lensrc = ft_strlen(src);
	if (lendst < size && lensrc > 0)
	{
		tmp = dst + lendst;
		tmp[size - lendst - 1] = 0;
		if (lensrc > size - lendst - 1)
			ft_strncpy(tmp, src, size - lendst - 1);
		else
			ft_strncpy(tmp, src, lensrc + 1);
	}
	else if (lendst > size)
		return (size + lensrc);
	return (lendst + lensrc);
}
