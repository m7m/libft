/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/20 19:16:28 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/30 05:43:24 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Trie pstr par ordre de la table ascii
*/

void	ft_strsort(char **pstr, int len_pstr)
{
	int		i;
	char	*str;
	char	*tmp;

	i = 0;
	str = *pstr;
	--len_pstr;
	while (i < len_pstr)
	{
		DEBUGSTR(pstr[i + 1]);
		DEBUGSTR(str);
		if (ft_strcmp(pstr[++i], str) > 0)
		{
			tmp = pstr[i];
			pstr[i] = str;
			str = tmp;
			i = 0;
			DEBUG;
		}
	}
	*pstr = str;
}

/*
** Trie une liste v2
*/

void	ft_strsortv2(char *list[])
{
	int		i;
	char	*tmp;

	i = 0;
	while (list[i] && list[i + 1])
	{
		if (ft_strcmp(list[i], list[i + 1]) > 0)
		{
			tmp = list[i + 1];
			list[i + 1] = list[i];
			list[i] = tmp;
			if (i > 0)
				--i;
			else
				++i;
		}
		else
			++i;
	}
}
