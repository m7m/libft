/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_chkalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/18 01:15:03 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/18 01:50:08 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_chkalloc(void *ptr)
{
	DEBUGPTR(ptr);
	if (!ptr)
		ft_exit(EXIT_FAILURE, MSG_OUT_OF_MEMORY);
	else
		ft_vtexit(ptr);
}
