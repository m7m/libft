/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_patexit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/18 01:47:27 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/18 02:00:22 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Enregistre f dans la static si non null
** si f est null: appel list f et vide la liste static
*/

/*
** Obsolete voir sttexit
*/

void	ft_patexit(void (*f)(void *))
{
	static void	(*listpfree[MAXLISTFREE + 1])(void *) = {NULL};
	static int	nbfree = -1;

	DEBUG;
	if (f)
	{
		if (nbfree >= MAXLISTFREE)
			ft_exit(EXIT_FAILURE, "overflow listfree");
		listpfree[++nbfree] = f;
	}
	else
		while (0 <= nbfree)
		{
			listpfree[nbfree](NULL);
			listpfree[nbfree] = NULL;
			--nbfree;
		}
}
