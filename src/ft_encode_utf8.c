/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_encode_utf8.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:07:40 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:07:40 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_encode_utf8__masq(wchar_t *enc)
{
	int	i;

	if (0x80 <= *enc && *enc <= 0x7FF)
	{
		*enc = (((*enc << 2) & 0x1F00) | (*enc & 0x3F)) | 0xC080;
		i = 2;
	}
	else if (0x800 <= *enc && *enc <= 0xFFFF)
	{
		*enc = ((((*enc << 4) & 0xF0000)
				| ((*enc << 2) & 0x3F00)) | (*enc & 0x3F)) | 0xE08080;
		i = 3;
	}
	else if (0x10000 <= *enc && *enc <= 0x10FFFF)
	{
		*enc = (((((*enc << 6) & 0x7000000)
				| ((*enc << 4) & 0x3F0000)) | ((*enc << 2) & 0x3F00))
				| (*enc & 0x3F)) | 0xF0808080;
		i = 4;
	}
	else
		i = 1;
	DEBUGNBR(i);
	return (i);
}

static void	ft_encode_utf8__wtoa(size_t len, wchar_t *enc)
{
	int		i;
	int		j;
	char	tmp;

	while (len--)
	{
		if (!(i = ft_isutf8(*enc)))
			i = ft_encode_utf8__masq(enc);
		j = 0;
		while (--i > j)
		{
			tmp = *((char *)enc + j);
			*((char *)enc + j) = *((char *)enc + i);
			*((char *)enc + i) = tmp;
			++j;
		}
		++enc;
	}
}

wchar_t		*ft_encode_utf8(wchar_t *mstr, size_t len)
{
	wchar_t	*e;

	if (mstr)
	{
		if (len == 1 && (e = (wchar_t *)ft_memalloc(2 * sizeof(wchar_t))))
			ft_memcpy(e, mstr, sizeof(wchar_t));
		else if (!len)
			return (ft_memalloc(sizeof(wchar_t)));
		else
			e = (wchar_t *)ft_memdup(mstr, (len + 1) * sizeof(wchar_t));
		mstr = e;
		if (e)
			ft_encode_utf8__wtoa(len, e);
	}
	DEBUGSTR((char *)mstr);
	return (mstr);
}
