/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base_s.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/28 19:53:16 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/28 19:53:16 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Convertie n en base
*/

int	ft_itoa_base_s(int n, char *base, char *dst)
{
	int		i;
	int		nbase;
	int		len;
	int		ret;

	if ((nbase = (int)ft_strlen(base)) < 2 || ft_strocu(base))
		return (-1);
	len = 1;
	i = n;
	while (i /= nbase)
		++len;
	if ((i = (n < 0)))
	{
		dst[0] = '-';
		dst[len] = base[-(n % nbase)];
		n = -(n / nbase);
	}
	dst[len + i] = 0;
	ret = len + i;
	--i;
	while (--len > i && (dst[len] = base[n % nbase]))
		n /= nbase;
	return (ret);
}
