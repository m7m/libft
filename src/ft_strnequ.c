/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 09:57:10 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/17 23:32:54 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Compare lexicographiquement s1 et s2 jusqu’à n caractères maximum
** ou bien qu’un ’\0’ ait été rencontré.
** Si les deux chaines sont égales, la fonction retourne 1, ou 0 sinon.
*/

#include <string.h>

int	ft_strnequ(char const *s1, char const *s2, size_t n)
{
	size_t i;

	i = 0;
	if (n == 0)
		return (1);
	while ((s1[i] || s2[i]) && s1[i] == s2[i] && i < n)
		i++;
	if (i == n)
		i--;
	if ((s1[i] - s2[i]) != 0)
		return (0);
	return (1);
}
