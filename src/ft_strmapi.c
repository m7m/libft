/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 09:21:07 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/03 18:13:02 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Applique la fonction f a chaque caractere de la chaine de caracteres
** passee en parametre en precisant son index en premier argument.
** Chaque caractere est passe par adresse a la fonction f
** afin de pouvoir etre modifie si necessaire.
*/

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char			*cps;
	unsigned int	len;
	unsigned int	i;

	i = 0;
	len = ft_strlen((char *)s);
	cps = (char *)malloc(sizeof(char) * (len + 1));
	if (!cps)
		return (NULL);
	while (i < len)
	{
		cps[i] = f(i, s[i]);
		i++;
	}
	cps[i] = 0;
	return (cps);
}
