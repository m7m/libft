/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 08:53:17 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/02 13:31:54 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Applique la fonction f a chaque caractere de la chaine de caracteres
** passee en parametre.
** Chaque caractere est passe par adresse a la fonction f
** afin de pouvoir etre modifie si necessaire.
** Retour: La chaine "fraiche" resultant des applications successives de f.
*/

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*cps;
	int		len;
	int		i;

	i = -1;
	len = ft_strlen((char *)s);
	cps = (char *)malloc(sizeof(char) * len + 1);
	if (!cps)
		return (NULL);
	while (++i < len)
		cps[i] = f(s[i]);
	cps[i] = 0;
	return (cps);
}
