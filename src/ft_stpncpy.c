/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stpncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/26 20:48:56 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/26 21:01:00 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Return pointer in '\0' dst
*/

#include <stddef.h>

char	*ft_stpncpy(char *dst, const char *src, size_t n)
{
	++n;
	while (--n)
		if (src[0])
		{
			*dst = src[0];
			++dst;
			++src;
		}
		else
		{
			*dst = 0;
			++dst;
		}
	return (dst);
}
