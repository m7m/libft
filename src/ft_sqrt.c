/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/19 19:22:12 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/26 12:39:13 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Retourne le carre de n
*/

int	ft_sqrt(int n)
{
	int	sqrt;

	sqrt = 0;
	while (sqrt * sqrt <= n)
		++sqrt;
	if (sqrt * sqrt == n)
		return (sqrt);
	else
		return (sqrt - 1);
}

/*
** Carre arrondie a la valeur superieur
*/

int	ft_sqrt_arr_sup(int n)
{
	int	sqrt;

	sqrt = 0;
	while (sqrt * sqrt < n)
		++sqrt;
	return (sqrt);
}
