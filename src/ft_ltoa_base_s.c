/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ltoa_base_s.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/28 20:01:33 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/28 20:01:33 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Convertie n en base
*/

long	ft_ltoa_base_s(long n, char *base, char *dst)
{
	long		i;
	long		nbase;
	long		len;
	long		ret;

	if ((nbase = (long)ft_strlen(base)) < 2 || ft_strocu(base))
		return (-1);
	len = 1;
	i = n;
	while (i /= nbase)
		++len;
	if ((i = (n < 0)))
	{
		dst[0] = '-';
		dst[len] = base[-(n % nbase)];
		n = -(n / nbase);
	}
	dst[len + i] = 0;
	ret = len + i;
	--i;
	while (--len > i && (dst[len] = base[n % nbase]))
		n /= nbase;
	return (ret);
}
