/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 08:03:52 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/05 02:45:32 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Supprime les espace/\n/\t en debut et en fin
*/
#include "libft.h"

char	*ft_strtrim(char const *s)
{
	int	lens;
	int	i;
	int	start;

	lens = ft_strlen(s);
	i = -1;
	start = 0;
	while (++i < lens)
		if (32 == s[i] || '\n' == s[i] || '\t' == s[i])
			start++;
		else
			i = lens;
	i = lens;
	if (start == lens)
		return ("");
	while (i-- > 0)
		if (32 == s[i] || '\n' == s[i] || '\t' == s[i])
			lens--;
		else
			i = -1;
	return (ft_strsub(s, start, lens - start));
}
