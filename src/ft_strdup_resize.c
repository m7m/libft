/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup_resize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/20 19:04:51 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/20 19:28:32 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Dupliaue s1 de sp plus grand
** puis retourne la nouvelle allocation
** is1 = length of s1
*/

char	*ft_strdup_resize(const char *s1, size_t is1, size_t sp)
{
	char *dus1;

	dus1 = (char *)malloc(sizeof(char) * (is1 + sp));
	if (dus1)
		ft_memcpy((void *)dus1, (void *)s1, is1);
	dus1[is1] = 0;
	return (dus1);
}
