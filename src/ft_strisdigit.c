/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strisdigit.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/17 15:54:30 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/17 15:54:30 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int
	ft_strisdigit(char *s)
{
	char	*ps;

	ps = s;
	while (*ps && ft_isdigit(*ps))
		++ps;
	if (s < ps && !*ps)
		return (1);
	return (0);
}
