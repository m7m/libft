/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_debug_retfd.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/13 23:40:43 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/13 23:40:43 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_debug.h"
#include <fcntl.h>

#include <stdio.h>

static void
	ft_retfd_close(void)
{
	close(ft_retfd());
}

int
	ft_retfd(void)
{
	static int	fd = -1;
	int			fd2;

	if (fd < 0)
	{
		if (sizeof(FILE_DEBUG_LOG) == sizeof(STDOUT_FILENO))
			fd = STDOUT_FILENO;
		else
		{
			fd = open(FILE_DEBUG_LOG, O_WRONLY | O_CREAT | O_APPEND, S_IRWXU);
			DEBUGNBR(fd);
			if (fd < 0)
				ft_exit(EXIT_FAILURE, "Error: open FILE_DEBUG_LOG\n");
			fd2 = dup2(fd, 99);
			if (fd2 < 0)
			{
				close(fd);
				ft_exit(EXIT_FAILURE, "Error: dup2 fd\n");
			}
			close(fd);
			fd = fd2;
			ft_atexit(ft_retfd_close);
		}
	}
	return (fd);
}

static FILE
	**ft_static_retstream(void)
{
	static FILE	*f[2] = {NULL, NULL};

	return (f);
}

static void
	ft_retstream_close(void)
{
	FILE	**f;

	f = ft_static_retstream();
	if (*f)
	{
		fclose(*f);
		*f = NULL;
	}
}

FILE
	*ft_retstream(void)
{
	FILE	**stf;
	FILE	*f;
	int		fd;

	stf = ft_static_retstream();
	f = *stf;
	if (!stf[1])
	{
		fd = ft_retfd();
		if (fd < 0)
			ft_exit(EXIT_FAILURE, "Error: ft_retstream -> fd FILE_DEBUG_LOG\n");
		f = fdopen(fd, "a");
		DEBUGPTR(f);
		if (!f)
			ft_exit(EXIT_FAILURE, "Error: fopen FILE_DEBUG_LOG\n");
		if (setvbuf(f, (char *)NULL, _IOLBF, 0))
			ft_exit(EXIT_FAILURE, "Error: ft_retstream -> setvbuf\n");
		ft_atexit(ft_retstream_close);
		*stf = f;
		stf[1] = f;
	}
	return (f);
}
