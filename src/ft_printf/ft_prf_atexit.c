/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prf_atexit.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:28:47 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/30 06:45:34 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_prf_atexit(void *ptrfree)
{
	static void	*listfree[MAXLISTFREE + 1] = {NULL};
	static int	nbfree = -1;

	DEBUG;
	if (ptrfree)
	{
		if (nbfree >= MAXLISTFREE)
			ft_exit(EXIT_FAILURE, "overflow MAXLISTFREE");
		listfree[++nbfree] = ptrfree;
	}
	else
		while (0 <= nbfree)
		{
			DEBUG;
			free(listfree[nbfree]);
			listfree[nbfree] = NULL;
			nbfree--;
			DEBUG;
		}
}
