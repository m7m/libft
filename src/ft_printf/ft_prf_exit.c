/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prf_exit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 20:56:24 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 20:56:25 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_prf_exit(int status, char *msg)
{
	DEBUG;
	if (msg)
	{
		if (status == EXIT_FAILURE)
			ft_putstr_fd(msg, STDERR_FILENO);
		else
			ft_putstr_fd(msg, STDOUT_FILENO);
	}
	ft_prf_atexit(NULL);
	DEBUGNBR(status);
	ft_exit(status, NULL);
}
