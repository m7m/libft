/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prf_atttree_osx.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:01:52 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:01:54 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Ne traite que les chiffres
*/

static int	ft_strict_atoi(char **ptrstr, t_prfarg *arg)
{
	char	*str;

	DEBUGSTR(*ptrstr);
	str = *ptrstr;
	arg->width = 0;
	while (*str > 47 && *str < 58)
	{
		arg->width = arg->width * 10 + *str - 48;
		++str;
	}
	*ptrstr = --str;
	DEBUGNBR(arg->width);
	return (1);
}

/*
** OS x
*/

static int	ft_prf_attlen_tree2(t_prf *prf, t_prfarg *arg)
{
	if (*prf->fmt == 'l' || *prf->fmt == 'q')
		ft_prf_lentree_ll(prf, arg);
	else if (*prf->fmt == 'h')
		ft_prf_lentree_hh(prf, arg);
	else if (*prf->fmt == 'L')
		arg->type |= FL_LEN_LLL;
	else if (*prf->fmt == 'j')
		arg->type |= FL_LEN_J;
	else if (*prf->fmt == 'z')
		arg->type |= FL_LEN_Z;
	else if (*prf->fmt == 't')
		arg->type |= FL_LEN_T;
	else
		return (0);
	return (1);
}

int			ft_prf_attlen_tree(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	while (*prf->fmt)
	{
		DEBUGC(*prf->fmt);
		if (((*prf->fmt == ' ' || *prf->fmt == '+')
			&& ft_prf_attr_sppl(prf, arg))
			|| (*prf->fmt == '-' && ft_prf_attr_mo(arg))
			|| (*prf->fmt == '#' && ft_prf_attr_co(arg))
			|| (*prf->fmt == '0' && ft_prf_attr_re(arg))
			|| (*prf->fmt == '*' && ft_prf_attr_mu(prf, arg))
			|| (*prf->fmt == '.' && ft_prf_attr_pr(prf, arg))
			|| (*prf->fmt == '$' && ft_prf_attr_or(prf, arg))
			|| (ft_isdigit(*prf->fmt) && ft_strict_atoi(&(prf->fmt), arg)))
			;
		else if (ft_prf_attlen_tree2(prf, arg))
			;
		else
			return (ft_prf_convtree(prf, arg));
		++prf->fmt;
	}
	return (0);
}
