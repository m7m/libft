/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_parse_len.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:00:21 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:50:26 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	ft_prf_lentree_err(t_prf *prf, int ret)
{
	int			i;
	t_prfarg	*arglast;
	t_prfarg	*arg;

	arglast = *prf->lstarg;
	while (arglast->next)
		arglast = arglast->next;
	DEBUGNBR(ret);
	DEBUGNBR(arglast->size);
	i = arglast->size;
	while (*(prf->fmt - i) != '%')
		++i;
	arg = ft_lstprf_delandnews(prf->lstarg, arglast);
	arg->type = FL_CONV_S;
	ft_check_alloc(arg->pc = ft_strsub(prf->fmt - i, 0,
										i - ret - arglast->size));
	arg->size = i - ret - arglast->size;
	arg->next = arglast;
	--prf->fmt;
	DEBUGSTR(arg->pc);
	DEBUGNBR(arg->size);
	DEBUGSTR(arg->next->pc);
	DEBUGSTR(prf->fmt);
}

static int	ft_prf_lentree_next(t_prf *prf, t_prfarg *arg, int ret)
{
	if (*prf->fmt && !ft_prf_convtree(prf, arg))
	{
		if (arg->type & (FL_LEN_LL | FL_LEN_HH))
			++ret;
		ft_prf_lentree_err(prf, ret);
	}
	else if (!*prf->fmt)
		return (0);
	return (1);
}

/*
** Linux
** last if [% hh]
*/

int			ft_prf_lentree(t_prf *prf, t_prfarg *arg)
{
	int	ret;

	DEBUGSTR(prf->fmt);
	ret = 1;
	if (*prf->fmt == 'l' || *prf->fmt == 'q')
		ft_prf_lentree_ll(prf, arg);
	else if (*prf->fmt == 'h')
		ft_prf_lentree_hh(prf, arg);
	else if (*prf->fmt == 'L')
		arg->type |= FL_LEN_LLL;
	else if (*prf->fmt == 'j')
		arg->type |= FL_LEN_J;
	else if (*prf->fmt == 'z')
		arg->type |= FL_LEN_Z;
	else if (*prf->fmt == 't')
		arg->type |= FL_LEN_T;
	else
		ret = 0;
	if (ret)
		++prf->fmt;
	return (ft_prf_lentree_next(prf, arg, ret));
}
