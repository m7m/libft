/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prf_typetree.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:18:17 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/29 23:51:54 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_prf_typetree2(t_prf *prf, t_prfarg *arg)
{
	if (arg->type & FL_CONV_DBL)
		ft_type_double(prf, arg);
	else if (arg->type & FL_CONV_P)
		ft_type_pvoid(prf, arg);
	else if (arg->type & FL_CONV_N)
		ft_type_px(prf, arg);
	else if (arg->type & FL_CONV_FD)
		ft_type_fd(prf, arg);
	else
	{
		ft_prf_exit(EXIT_FAILURE, "Unrecognized conversion");
		return (0);
	}
	return (1);
}

static void	ft_prf_typetree_boux(t_prf *prf, t_prfarg *arg)
{
	if (arg->type & FL_LEN_LL)
		ft_type_ulonglong(prf, arg);
	else if (arg->type & FL_LEN_L)
		ft_type_ulong(prf, arg);
	else if (arg->type & FL_LEN_J)
		ft_type_uintmax_t(prf, arg);
	else if (arg->type & FL_LEN_Z)
		ft_type_usize_t(prf, arg);
	else if (arg->type & FL_LEN_T)
		ft_type_uptrdiff_t(prf, arg);
	else if (arg->type & FL_LEN_H)
		ft_type_ushort(prf, arg);
	else if (arg->type & FL_LEN_HH)
		ft_type_uchar(prf, arg);
	else
		ft_type_uint(prf, arg);
}

static void	ft_prf_typetree_di(t_prf *prf, t_prfarg *arg)
{
	if (arg->type & FL_LEN_LL)
		ft_type_longlong(prf, arg);
	else if (arg->type & FL_LEN_L)
		ft_type_long(prf, arg);
	else if (arg->type & FL_LEN_J)
		ft_type_intmax_t(prf, arg);
	else if (arg->type & FL_LEN_Z)
		ft_type_size_t(prf, arg);
	else if (arg->type & FL_LEN_T)
		ft_type_ptrdiff_t(prf, arg);
	else if (arg->type & FL_LEN_H)
		ft_type_short(prf, arg);
	else if (arg->type & FL_LEN_HH)
		ft_type_charshort(prf, arg);
	else
		ft_type_int(prf, arg);
}

int			ft_prf_typetree(t_prf *prf, t_prfarg *arg)
{
	DEBUGPTR(arg);
	DEBUGNBR(arg->type);
	DEBUGNBR(arg->ordre_ap);
	DEBUGNBR(prf->nbap);
	if (arg->type & FL_LEN_L && arg->type & FL_CONV_S)
		ft_type_pwchar_t(prf, arg);
	else if (arg->type & FL_LEN_L && arg->type & FL_CONV_C)
		ft_type_wchar_t(prf, arg);
	else if (arg->type & FL_CONV_S)
		ft_type_pchar(prf, arg);
	else if (arg->type & FL_CONV_C)
		ft_type_char(prf, arg);
	else if (arg->type & FL_CONV_DI)
		ft_prf_typetree_di(prf, arg);
	else if (arg->type & FL_CONV_BOUX)
		ft_prf_typetree_boux(prf, arg);
	else if (!ft_prf_typetree2(prf, arg))
		return (0);
	arg->conv_flags |= arg->type;
	arg->type = FL_CONV_S;
	return (1);
}
