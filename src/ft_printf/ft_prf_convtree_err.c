/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prf_convtree_err.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 20:57:42 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 20:57:42 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			ft_prf_convtree_err(t_prf *prf, t_prfarg *arg)
{
	int		i;

	DEBUG;
	i = 0;
	while (*(prf->fmt + i) != '%' && *(prf->fmt + i) != 0)
		++i;
	arg = ft_lstprf_delandnews(prf->lstarg, arg);
	arg->type = FL_CONV_S;
	if (*(prf->fmt + i) && *(prf->fmt + i) == '%' && !*(prf->fmt + i + 1))
		++i;
	ft_check_alloc(arg->pc = ft_strsub(prf->fmt, 0, i));
	arg->size = i;
	prf->fmt += i;
	DEBUGSTR(prf->fmt);
	DEBUGSTR(arg->pc);
	return (0);
}
