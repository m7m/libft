/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_parse.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 16:41:49 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:30:23 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	ft_printf_parse_str(t_prf *prf, char *str)
{
	t_prfarg	*arg;

	DEBUGSTR(str);
	arg = ft_lstprf_newadd(prf->lstarg);
	arg->size = prf->fmt - str;
	ft_check_alloc(arg->pc = ft_strsub(str, 0, arg->size));
	arg->type = FL_CONV_S;
}

static char	*ft_printf_parse_p(t_prf *prf, char **str)
{
	DEBUGSTR(prf->fmt);
	if (*(prf->fmt + 1) != '%')
	{
		if (*str < prf->fmt)
			ft_printf_parse_str(prf, *str);
		if (ft_prf_parse_attchr(prf))
			*str = prf->fmt + 1;
		else
			*str = prf->fmt;
	}
	else if (*(prf->fmt + 1) == '%')
	{
		DEBUG;
		if (*str < prf->fmt)
			ft_printf_parse_str(prf, *str);
		*str = ++prf->fmt;
	}
	return (*str);
}

int			ft_printf_parse(t_prf *prf)
{
	char	*str;

	str = prf->fmt;
	DEBUGSTR(str);
	while (*(prf->fmt = ft_strchrnull(prf->fmt, '%')))
	{
		ft_printf_parse_p(prf, &str);
		DEBUGSTR(prf->fmt);
		if (*prf->fmt)
			++prf->fmt;
	}
	DEBUGSTR(prf->fmt);
	DEBUGSTR(str);
	if (str < prf->fmt)
		ft_printf_parse_str(prf, str);
	return (1);
}
