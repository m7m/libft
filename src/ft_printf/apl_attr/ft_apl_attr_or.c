/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_apl_attr_or.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 22:30:01 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 22:30:25 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** $
*/

void	ft_apl_attr_or(t_prfarg *arg)
{
	DEBUGPTR(arg->pc_arg);
	if (arg->pc_arg)
	{
		DEBUGSTR(arg->pc);
		DEBUGNBR(arg->u.i);
		while (arg->pc_arg->pc_arg)
		{
			DEBUG;
			arg->pc_arg = arg->pc_arg->pc_arg;
		}
		DEBUGNBR(arg->pc_arg->conv_flags);
		DEBUGNBR(arg->type);
		DEBUGNBR(arg->conv_flags);
		if (ft_ptype(arg, arg->pc_arg, arg->type))
			return ;
		if (!arg->size)
			arg->size = ft_strlen(arg->pc);
		arg->type = FL_CONV_S;
		DEBUGSTR(arg->pc);
	}
}
