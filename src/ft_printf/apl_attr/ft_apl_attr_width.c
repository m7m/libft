/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_apl_attr_width.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 22:05:35 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 22:09:29 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	ft_apl_attr_width__arg(t_prfarg *arg)
{
	int	i;

	DEBUGPTR(arg->width_arg);
	i = 0;
	if (arg->width_arg->pc_arg)
		ft_ptype(arg->width_arg, arg->width_arg->pc_arg,
				arg->width_arg->type);
	i = ft_ptype_int(arg->width_arg);
	DEBUGNBR(i);
	if (i < 0)
	{
		arg->sens_width = 1;
		arg->remplissage = ' ';
	}
	if (!arg->width)
		arg->width = i;
	if (arg->width < 0)
	{
		arg->width = -arg->width;
		arg->sens_width = 1;
		arg->remplissage = ' ';
	}
	DEBUGNBR(arg->width);
	DEBUGSTR(arg->width_arg->pc);
}

void		ft_apl_attr_width(t_prfarg *arg)
{
	char	*str;

	DEBUGSTR(arg->pc);
	DEBUGSTR(arg->add_remplissage);
	if (arg->width_arg)
		ft_apl_attr_width__arg(arg);
	if (*arg->add_remplissage && arg->remplissage == ' ')
		ft_check_alloc(arg->pc = ft_strjoin(arg->add_remplissage, arg->pc));
	arg->size += arg->size_add;
	DEBUGNBR(arg->width);
	DEBUGC(arg->remplissage);
	if (arg->width > arg->size)
	{
		ft_check_alloc(str = (char *)ft_memallocset(
							sizeof(char) * (arg->width),
							arg->remplissage));
		if (arg->sens_width)
			ft_strncpy(str, arg->pc, arg->size);
		else
			ft_strcpy(str + (arg->width - arg->size), arg->pc);
		arg->pc = str;
		arg->size = arg->width;
	}
	if (*arg->add_remplissage && arg->remplissage != ' ')
		ft_check_alloc(arg->pc = ft_strjoin(arg->add_remplissage, arg->pc));
}
