/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_apl_attr_co.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 22:28:10 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 22:28:11 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** #
*/

void		ft_apl_attr_co(t_prfarg *arg)
{
	char	*r;

	r = arg->add_remplissage;
	while (*r)
		++r;
	arg->size_add = 2;
	if (arg->conv_c == 'x')
		ft_strncpy(r, "0x", 2);
	else if (arg->conv_c == 'X')
		ft_strncpy(r, "0X", 2);
	else if ((arg->conv_c == 'o' || arg->conv_c == 'O')
			&& arg->preci <= arg->size)
	{
		arg->size_add = 0;
		arg->preci = arg->size + 1;
		arg->attr_flags |= FL_PR;
	}
	else if (arg->conv_c == 'b')
		ft_strncpy(r, "0b", 2);
	else if (arg->conv_c == 'B')
		ft_strncpy(r, "0B", 2);
	else
		arg->size_add = 0;
	DEBUGSTR(arg->add_remplissage);
	DEBUGNBR(arg->size_add);
}
