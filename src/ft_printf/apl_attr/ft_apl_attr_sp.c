/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_apl_attr_sp.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 22:29:04 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 22:29:11 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** ' '
*/

void		ft_apl_attr_sp(t_prfarg *arg)
{
	DEBUG;
	if (LINUX)
	{
		if (!(arg->conv_flags & (FL_CONV_DI | FL_CONV_DBL | FL_CONV_P))
			|| *arg->pc == '-')
			return ;
	}
	else if (!(arg->conv_flags & (FL_CONV_DI | FL_CONV_DBL))
			|| *arg->pc == '-')
		return ;
	DEBUG;
	*arg->add_remplissage = ' ';
	DEBUG;
	++arg->size;
	if (arg->attr_flags & FL_PR)
		++arg->preci;
}
