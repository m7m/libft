/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_apl_attr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 22:39:14 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 22:39:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	ft_apl_attrtree(t_prfarg *arg)
{
	DEBUGNBR(arg->size);
	DEBUGNBR(arg->width);
	DEBUGNBR(arg->type);
	DEBUGNBR(arg->conv_flags);
	DEBUGNBR(arg->attr_flags);
	if (arg->attr_flags & FL_OR)
		ft_apl_attr_or(arg);
	if (arg->attr_flags & FL_PL || (arg->pc && *arg->pc == '-'))
		ft_apl_attr_pl(arg);
	else if (arg->attr_flags & FL_SP)
		ft_apl_attr_sp(arg);
	if (arg->attr_flags & FL_CO
		&& !(arg->conv_flags & (FL_CONVNOTATT_CO))
		&& (arg->conv_flags & FL_CONV_AA
			|| arg->conv_flags & FL_CONV_P
			|| *arg->pc != 48))
		ft_apl_attr_co(arg);
	if (arg->attr_flags & FL_MO)
		ft_apl_attr_mo(arg);
	else if (arg->attr_flags & FL_RE)
		ft_apl_attr_re(arg);
	if (arg->attr_flags & FL_PR)
		ft_apl_attr_pr(arg);
	ft_apl_attr_width(arg);
	DEBUGSTR(arg->pc);
}

void		ft_apl_attr(t_prf *prf)
{
	t_prfarg	*arg;

	DEBUG;
	arg = *prf->lstarg;
	while (arg)
	{
		DEBUGPTR(arg);
		DEBUGNBR(arg->ordre_ap);
		DEBUGSTR(arg->pc);
		DEBUGPTR(arg->pc_arg);
		DEBUGNBR(arg->attr_flags);
		DEBUGNBR(arg->type);
		DEBUGNBR(arg->conv_flags);
		if (arg->attr_flags & ~FL_NONE || arg->width)
			ft_apl_attrtree(arg);
		DEBUGSTR(arg->pc);
		DEBUGNBR(arg->type);
		arg = arg->next;
	}
	DEBUG;
}
