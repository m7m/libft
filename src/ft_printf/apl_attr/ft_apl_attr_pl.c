/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_apl_attr_pl.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 22:28:36 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 22:32:18 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** + et négatif
*/

void		ft_apl_attr_pl(t_prfarg *arg)
{
	DEBUG;
	if (!(arg->conv_flags & FL_CONV_NUM)
		|| arg->conv_flags & FL_CONV_BOUX
		|| (arg->conv_flags & FL_CONV_P && !LINUX))
	{
		DEBUG;
		arg->attr_flags &= ~FL_PL;
		return ;
	}
	if (*arg->pc == '-')
	{
		if (*arg->add_remplissage)
			ft_memcpy(arg->add_remplissage + 1,
					arg->add_remplissage, sizeof(char) * 7);
		++arg->pc;
		*arg->add_remplissage = '-';
		--arg->size;
	}
	else
		*arg->add_remplissage = '+';
	if (arg->attr_flags & FL_PR)
		++arg->preci;
	++arg->size;
	DEBUG;
}
