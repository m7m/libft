/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_apl_attr_re.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 22:29:45 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 22:31:48 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** 0
*/

void		ft_apl_attr_re(t_prfarg *arg)
{
	DEBUG;
	if (LINUX)
	{
		if ((!(arg->attr_flags & FL_PR)
			|| arg->conv_flags & FL_CONV_DBL)
			&& !(arg->conv_flags & (FL_CONV_S | FL_CONV_C)))
			arg->remplissage = '0';
	}
	else
	{
		if (!(arg->attr_flags & FL_PR)
			|| arg->conv_flags & FL_CONV_DBL
			|| arg->conv_flags & (FL_CONV_S | FL_CONV_C))
			arg->remplissage = '0';
	}
	DEBUGC(arg->remplissage);
}
