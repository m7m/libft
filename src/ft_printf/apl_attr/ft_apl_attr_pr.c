/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_apl_attr_pr.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 22:12:56 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 22:25:51 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	ft_apl_attr_pr__arg_dbl(int p_preci, t_prfarg *arg)
{
	if (p_preci >= 0)
		arg->preci = p_preci;
	else
	{
		arg->preci = 6;
		arg->attr_flags &= ~FL_PR;
	}
	arg->type = arg->conv_flags;
	ft_type_double_fmt(arg);
	arg->size = ft_strlen(arg->pc);
	if (arg->pc && *arg->pc == '-')
		ft_apl_attr_pl(arg);
	if (arg->attr_flags & FL_CO)
		ft_apl_attr_co(arg);
	arg->preci = -1;
	if (arg->preci_arg != arg)
		arg->preci_arg->type = 0;
}

static void	ft_apl_attr_pr__arg_neg(t_prfarg *arg)
{
	if (arg->conv_flags & (FL_CONV_C | FL_CONV_S))
	{
		if (arg->width > arg->size)
			arg->preci = arg->width;
		else
			arg->preci = arg->size;
	}
	else
	{
		if (arg->attr_flags & FL_RE)
			arg->preci = arg->width;
		else
			arg->preci = arg->size;
	}
}

/*
** Precision fournit dans un autre arg
*/

static void	ft_apl_attr_pr__arg(t_prfarg *arg)
{
	int	p_preci;

	DEBUGNBR(arg->preci);
	DEBUGNBR(arg->preci_arg->u.i);
	if (arg->preci_arg->pc_arg)
		ft_ptype(arg->preci_arg, arg->preci_arg->pc_arg,
				arg->preci_arg->type);
	p_preci = ft_ptype_int(arg->preci_arg);
	DEBUGNBR(p_preci);
	DEBUGNBR(arg->preci);
	if (arg->conv_flags & FL_CONV_DBL)
		ft_apl_attr_pr__arg_dbl(p_preci, arg);
	else if (p_preci > 0)
		arg->preci = p_preci;
	else if (p_preci < 0)
		ft_apl_attr_pr__arg_neg(arg);
	else
		arg->preci = 0;
	DEBUGNBR(arg->preci);
	DEBUGSTR(arg->preci_arg->pc);
	DEBUGSTR(arg->pc);
}

/*
** Traitement pour correlation avec la fonction reel
*/

static void	ft_apl_attr_pr__2(t_prfarg *arg, char *str)
{
	if (arg->preci > arg->size
			&& !(arg->conv_flags & (FL_CONV_S | FL_CONV_LS)))
	{
		DEBUG;
		str = (char *)ft_memallocset(sizeof(char) * (arg->preci + 1), 48);
		ft_check_alloc(str);
		ft_strcpy(str + (arg->preci - arg->size), arg->pc);
		arg->pc = str;
		arg->size = arg->preci;
	}
	else if (arg->attr_flags & FL_CO
			&& (arg->conv_c == 'o' || arg->conv_c == 'O'))
		;
	else if (!arg->preci && arg->conv_flags & FL_CONV_NUM
			&& *arg->pc == 48 && arg->size == 1)
	{
		arg->pc = "";
		arg->size = 0;
	}
}

/*
** .
*/

void		ft_apl_attr_pr(t_prfarg *arg)
{
	if (!arg->pc)
	{
		arg->pc = "";
		arg->size = 0;
	}
	if (arg->preci_arg)
		ft_apl_attr_pr__arg(arg);
	if (arg->conv_flags & (FL_CONV_C | FL_CONV_LC)
		|| (MSGNULLPV != 0 && !ft_strcmp(MSGNULLPV, arg->pc)))
	{
		DEBUG;
		arg->preci = 0;
		return ;
	}
	if (arg->preci < arg->size && !(arg->conv_flags & FL_CONV_NUM))
	{
		if (ENCODAGE && UTF8)
			while (arg->preci && arg->pc[arg->preci] & (1 << 7)
				&& arg->pc[arg->preci] && !(arg->pc[arg->preci] & (1 << 6)))
				--arg->preci;
		arg->size = arg->preci;
		arg->pc[arg->preci] = 0;
	}
	else
		ft_apl_attr_pr__2(arg, NULL);
}
