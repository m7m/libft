/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_alloc.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 20:57:48 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 20:58:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_check_alloc(void *ptr)
{
	DEBUGPTR(ptr);
	if (!ptr)
		ft_prf_exit(EXIT_FAILURE, MSG_OUT_OF_MEMORY);
	else
		ft_prf_atexit(ptr);
}
