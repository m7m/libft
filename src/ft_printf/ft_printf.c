/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:29:49 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/30 00:11:13 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void
	ft_printf__nbr_write(t_prf *prf)
{
	DEBUG;
	*(int *)prf->pnb_write = prf->nb_write;
	DEBUG;
}

static void
	ft_printf__init_prf(t_prf *prf)
{
	*(prf->lstarg) = NULL;
	prf->nb_write = 0;
	prf->nbap = 0;
	prf->pnb_write = NULL;
	prf->nbap_max = 0;
	prf->pnb_write = NULL;
	prf->fd = STDOUT_FILENO;
	prf->fd_flush = 0;
}

int	ft_printf(const char *format, ...)
{
	t_prf	prf[1];
	int		nb_write;

	DEBUG;
	if (!format)
		return (0);
	ft_printf__init_prf(prf);
	prf->fmt = (char *)format;
	DEBUGPTR(prf->fmt);
	va_start(prf->ap, format);
	ft_printf_parse(prf);
	DEBUGPTR(prf);
	ft_printf_arg(prf);
	DEBUG;
	va_end(prf->ap);
	DEBUG;
	nb_write = prf->nb_write;
	DEBUG;
	if (prf->pnb_write)
		ft_printf__nbr_write(prf);
	if (prf->fd_flush & 0b10)
		ft_fflush(prf->fd);
	ft_prf_atexit(NULL);
	DEBUGNBR(nb_write);
	return (nb_write);
}
