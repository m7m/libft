/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prf_lentree_.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:19:39 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:19:39 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		ft_prf_lentree_hh(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	if (*(prf->fmt + 1) == 'h')
	{
		++prf->fmt;
		arg->type |= FL_LEN_HH;
	}
	else
		arg->type |= FL_LEN_H;
}

void		ft_prf_lentree_ll(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	if (*(prf->fmt + 1) == 'l')
	{
		DEBUG;
		++prf->fmt;
		arg->type |= FL_LEN_LL;
	}
	else
		arg->type |= FL_LEN_L;
}
