/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsort_arg.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:17:57 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:17:58 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** trie les argument dans l'ordre ordre_ap
** voir * et/ou $
*/

static t_prfarg	*ft_lstsort_arg__type(t_prfarg *lstarg, t_prfarg *arg)
{
	DEBUGPTR(arg);
	while (lstarg)
	{
		if (arg->ordre_ap == lstarg->ordre_ap
			&& (arg->type >> 14) < (lstarg->type >> 14))
		{
			DEBUGPTR(lstarg);
			if (!arg->pc_arg)
			{
				arg->pc_arg = lstarg;
				arg->attr_flags |= FL_OR;
			}
			arg = lstarg;
			arg->pc_arg = NULL;
			arg->attr_flags &= ~FL_OR;
		}
		lstarg = lstarg->next;
	}
	return (arg);
}

static t_prfarg	*ft_lstsort_arg__null(t_prfarg *lstarg[1], t_prfarg *arg, int i)
{
	DEBUG;
	arg = ft_lstprf_newadd(lstarg);
	arg->ordre_ap = i;
	arg->type = FL_CONV_DI;
	arg->attr_flags = FL_NONE;
	arg->size = -1;
	return (arg);
}

static t_prfarg	*ft_lstsort_arg__while(t_prf *prf, t_prfarg *argsort[1],
										t_prfarg *arg, t_prfarg *argtmp)
{
	int	i;

	i = 1;
	while (i <= prf->nbap)
	{
		if (!arg)
			arg = ft_lstsort_arg__null(prf->lstarg, arg, i);
		if (arg->ordre_ap == i && !arg->pc_arg)
		{
			arg = ft_lstsort_arg__type(*prf->lstarg, arg);
			DEBUGNBR(arg->ordre_ap);
			if (!argtmp)
				*argsort = arg;
			else
				argtmp->nextsort = arg;
			argtmp = arg;
			arg = *prf->lstarg;
			++i;
		}
		else
			arg = arg->next;
	}
	if (i < prf->nbap)
		ft_prf_exit(EXIT_FAILURE, "Error in order argument");
	return (*argsort);
}

t_prfarg		*ft_lstsort_arg(t_prf *prf)
{
	t_prfarg	*argsort[1];

	DEBUGNBR(prf->nbap);
	*argsort = NULL;
	*argsort = ft_lstsort_arg__while(prf, argsort, *prf->lstarg, NULL);
	return (*argsort);
}
