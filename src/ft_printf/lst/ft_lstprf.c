/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstprf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:16:48 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:16:55 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static t_prfarg	*ft_lstprf_new(void)
{
	t_prfarg	*arg;

	DEBUG;
	if (!(arg = (t_prfarg *)ft_memalloc(sizeof(t_prfarg))))
		ft_prf_exit(EXIT_FAILURE, MSG_OUT_OF_MEMORY);
	arg->next = NULL;
	arg->remplissage = ' ';
	ft_prf_atexit(arg);
	DEBUGPTR(arg);
	return (arg);
}

t_prfarg		*ft_lstprf_newadd(t_prfarg **lstprf)
{
	t_prfarg	*new;
	t_prfarg	*lsttmp;

	DEBUG;
	new = ft_lstprf_new();
	if (*lstprf)
	{
		lsttmp = *lstprf;
		while (lsttmp->next)
			lsttmp = lsttmp->next;
		lsttmp->next = new;
	}
	else
		*lstprf = new;
	DEBUG;
	return (new);
}

t_prfarg		*ft_lstarg_isexist(t_prfarg **lstarg, int ordre_ap)
{
	t_prfarg	*tmp;

	DEBUGNBR(ordre_ap);
	tmp = *lstarg;
	while (tmp)
	{
		if (ordre_ap == tmp->ordre_ap)
			return (tmp);
		tmp = tmp->next;
	}
	DEBUG;
	return (NULL);
}
