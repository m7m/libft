/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstprf_delandnews.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:17:09 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:17:10 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_prfarg	*ft_lstprf_delandnews(t_prfarg **lstarg, t_prfarg *arg)
{
	t_prfarg	*argtmp;

	DEBUGPTR(arg);
	argtmp = *lstarg;
	if (argtmp && arg)
	{
		if (!argtmp->next || argtmp == arg)
			*lstarg = NULL;
		else
		{
			while (argtmp->next && argtmp->next != arg)
				argtmp = argtmp->next;
			if (argtmp->next && argtmp->next->next)
			{
				arg = argtmp->next->next;
				argtmp->next = arg;
				arg->next = NULL;
			}
			else if (argtmp->next)
				argtmp->next = NULL;
			else
				ft_exit(EXIT_FAILURE, "ft_lstprf_delandnews: boucle infini\n");
		}
	}
	return (ft_lstprf_newadd(lstarg));
}
