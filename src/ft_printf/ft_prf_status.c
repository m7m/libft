/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prf_status.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:29:38 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:35:07 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_status.h"

t_status		*ft_prf_status(int status, char *msg)
{
	static t_status	st[1] = {{NULL, 0, 0}};

	if (msg)
	{
		st->status = status;
		st->msg = msg;
		++st->pass;
	}
	return (st);
}
