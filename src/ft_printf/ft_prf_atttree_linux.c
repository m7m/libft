/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prf_atttree_linux.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:35:23 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:35:24 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	ft_printf_parse_att_pour(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg = ft_lstprf_delandnews(prf->lstarg, arg);
	arg->attr_flags = FL_NONE;
	arg->size = -1;
	DEBUG;
	arg->type = FL_CONV_S;
}

/*
** Ne traite que les chiffres
*/

static int	ft_strict_atoi(char **ptrstr, t_prfarg *arg)
{
	char	*str;

	DEBUGSTR(*ptrstr);
	str = *ptrstr;
	if (arg->width
		|| arg->attr_flags & (FL_MU)
		|| *(str - 1) == '*')
	{
		return (0);
	}
	arg->width = 0;
	while (*str > 47 && *str < 58)
	{
		arg->width = arg->width * 10 + *str - 48;
		++str;
	}
	*ptrstr = --str;
	DEBUGNBR(arg->width);
	return (1);
}

/*
** Linux
*/

int			ft_prf_atttree(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	while (*prf->fmt)
		if (((*prf->fmt == ' ' || *prf->fmt == '+')
			&& ft_prf_attr_sppl(prf, arg))
			|| (*prf->fmt == '-' && ft_prf_attr_mo(arg))
			|| (*prf->fmt == '#' && ft_prf_attr_co(arg))
			|| (*prf->fmt == '0' && ft_prf_attr_re(arg))
			|| (*prf->fmt == '*' && ft_prf_attr_mu(prf, arg))
			|| (*prf->fmt == '.' && ft_prf_attr_pr(prf, arg))
			|| (*prf->fmt == '$' && ft_prf_attr_or(prf, arg))
			|| (ft_isdigit(*prf->fmt) && ft_strict_atoi(&(prf->fmt), arg)))
			++prf->fmt;
		else if (*prf->fmt == '%')
		{
			DEBUGSTR(prf->fmt);
			ft_printf_parse_att_pour(prf, arg);
			return (0);
		}
		else
		{
			DEBUGSTR(prf->fmt);
			DEBUGNBR(arg->width);
			return (ft_prf_lentree(prf, arg));
		}
	return (0);
}
