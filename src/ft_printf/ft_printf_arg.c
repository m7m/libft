/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_arg.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 16:41:23 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:31:55 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	ft__arg_write__(t_prf *prf, t_prfarg *arg)
{
	if (prf->fd_flush & 0b1)
	{
		ft_fflush(prf->fd);
		write(prf->fd, arg->pc, arg->size);
	}
	else
		ft_buff(prf->fd, arg->pc, arg->size);
}

static void	ft__arg_write(t_prf *prf)
{
	t_prfarg	*arg;

	arg = *prf->lstarg;
	while (arg)
	{
		DEBUGSTR(arg->pc);
		DEBUGNBR(arg->size);
		if (arg->type == FL_CONV_S && !(arg->attr_flags & FL_NONE))
		{
			if (prf->nb_write < 0)
				prf->nb_write = arg->size;
			else if (arg->size > 0)
				prf->nb_write += arg->size;
			if (arg->size > 0)
				ft__arg_write__(prf, arg);
		}
		else if (!prf->nb_write && !(arg->attr_flags & FL_NONE))
		{
			if (!LINUX)
				prf->nb_write = 0;
			else
				prf->nb_write = -1;
		}
		arg = arg->next;
	}
}

void		ft_printf_arg(t_prf *prf)
{
	int			nb_arg_type;
	t_prfarg	*arg;

	if (prf->nbap < prf->nbap_max)
		prf->nbap = prf->nbap_max;
	DEBUGNBR(prf->nbap);
	nb_arg_type = 0;
	arg = ft_lstsort_arg(prf);
	while (arg)
	{
		nb_arg_type += ft_prf_typetree(prf, arg);
		if (!arg->size)
			arg->size = ft_strlen(arg->pc);
		arg = arg->nextsort;
	}
	DEBUG;
	ft_apl_attr(prf);
	ft__arg_write(prf);
	DEBUG;
}
