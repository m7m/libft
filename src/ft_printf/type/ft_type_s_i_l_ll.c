/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_type_s_i_l_ll.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:26:11 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:26:21 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_type_short(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.i = va_arg(prf->ap, int);
	ft_check_alloc(arg->pc = ft_stoa(arg->u.s));
	DEBUGSTR(arg->pc);
}

void	ft_type_int(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.i = va_arg(prf->ap, int);
	ft_check_alloc(arg->pc = ft_itoa(arg->u.i));
	DEBUGSTR(arg->pc);
}

void	ft_type_long(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.l = va_arg(prf->ap, long);
	ft_check_alloc(arg->pc = ft_ltoa(arg->u.l));
	DEBUGSTR(arg->pc);
}

void	ft_type_longlong(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.ll = va_arg(prf->ap, long long);
	ft_check_alloc(arg->pc = ft_lltoa(arg->u.ll));
	DEBUGSTR(arg->pc);
}
