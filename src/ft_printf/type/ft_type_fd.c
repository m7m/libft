/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_type_fd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/13 19:04:46 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/13 19:04:46 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_type_fd(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	prf->fd = va_arg(prf->ap, int);
	if (prf->fd < 0)
		ft_prf_exit(EXIT_FAILURE, "Error: fd invalide");
	if (arg->width)
		prf->fd_flush = 1;
	arg->width = 0;
	DEBUGSTR(arg->pc);
}
