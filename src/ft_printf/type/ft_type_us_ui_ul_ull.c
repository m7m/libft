/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_type_us_ui_ul_ull.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:20:18 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:20:19 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_type_ushort(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.i = va_arg(prf->ap, int);
	ft_check_alloc(arg->pc = ft_ustoa_base(arg->u.us, arg->base));
	DEBUGSTR(arg->pc);
}

void	ft_type_uint(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.ui = va_arg(prf->ap, unsigned int);
	ft_check_alloc(arg->pc = ft_uitoa_base(arg->u.ui, arg->base));
	DEBUGSTR(arg->pc);
}

void	ft_type_ulong(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.ul = va_arg(prf->ap, unsigned long);
	ft_check_alloc(arg->pc = ft_ultoa_base(arg->u.ul, arg->base));
	DEBUGSTR(arg->pc);
}

void	ft_type_ulonglong(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.ull = va_arg(prf->ap, unsigned long long);
	ft_check_alloc(arg->pc = ft_ulltoa_base(arg->u.ull, arg->base));
	DEBUGSTR(arg->pc);
}
