/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_type_im_uim.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:26:06 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:26:06 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_type_intmax_t(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.im = va_arg(prf->ap, intmax_t);
	ft_check_alloc(arg->pc = ft_imtoa_base(arg->u.im, arg->base));
	DEBUGSTR(arg->pc);
}

void	ft_type_uintmax_t(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.uim = va_arg(prf->ap, uintmax_t);
	ft_check_alloc(arg->pc = ft_uimtoa_base(arg->u.uim, arg->base));
	DEBUGSTR(arg->pc);
}
