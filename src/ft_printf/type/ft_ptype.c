/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ptype.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:29:19 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:29:19 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	ft_ptype_di(t_prfarg *arg, t_prfarg *parg, t_ui type)
{
	DEBUG;
	if (type & FL_LEN_H)
	{
		DEBUG;
		arg->pc = ft_stoa((short)parg->u.s);
	}
	else if (type & FL_LEN_HH)
	{
		DEBUG;
		arg->pc = ft_ctoa_base(parg->u.c, "0123456789");
	}
	else if (type & FL_LEN_L)
	{
		DEBUG;
		arg->pc = ft_ltoa((long)parg->u.l);
	}
	else if (type & FL_LEN_LL)
	{
		DEBUG;
		arg->pc = ft_lltoa((t_ll)parg->u.ll);
	}
	else
		arg->pc = ft_itoa(parg->u.i);
}

static void	ft_ptype_boux(t_prfarg *arg, t_prfarg *parg, t_ui type)
{
	DEBUG;
	if (type & FL_LEN_H)
	{
		DEBUG;
		arg->pc = ft_ustoa_base((t_us)parg->u.us, arg->base);
	}
	else if (type & FL_LEN_HH)
	{
		DEBUG;
		arg->pc = ft_uctoa_base(parg->u.uc, arg->base);
	}
	else if (type & FL_LEN_L)
	{
		DEBUG;
		arg->pc = ft_ultoa_base((t_ul)parg->u.ul, arg->base);
	}
	else if (type & FL_LEN_LL)
	{
		DEBUG;
		arg->pc = ft_ulltoa_base((t_ull)parg->u.ull, arg->base);
	}
	else
		arg->pc = ft_uitoa_base((t_ui)parg->u.ui, arg->base);
}

static void	ft_ptype_c(t_prfarg *arg, t_prfarg *parg, t_ui type)
{
	char	str[2];

	DEBUG;
	if (type & FL_LEN_L)
	{
		DEBUG;
		arg->pc = ft_wctoa(parg->u.wc);
	}
	else
	{
		DEBUGC(parg->u.c);
		str[0] = parg->u.c;
		str[1] = 0;
		arg->size = 1;
		arg->pc = (char *)ft_memdup(str, sizeof(char) * 2);
	}
}

static void	ft_ptype_s(t_prfarg *arg, t_prfarg *parg, t_ui type)
{
	char	*str;

	DEBUG;
	if (type & FL_LEN_L)
	{
		DEBUG;
		if (!(str = ft_memalloc(
				sizeof(char) * (ft_strlen((char *)parg->u.pv) + 5))))
			ft_prf_exit(EXIT_FAILURE, MSG_OUT_OF_MEMORY);
		arg->pc = ft_wctoa(parg->u.wc);
		free(str);
	}
	else
	{
		arg->pc = ft_strdup((char *)parg->u.pv);
	}
}

/*
** static void	ft_ptype_dbl(t_prfarg *arg, t_prfarg *parg)
** {
** 	int	preci;
** 	char	*str;
** 	DEBUG;
** 	preci = parg->preci;
** 	str = parg->pc;
** 	parg->preci = 0;
** 	ft_type_double_fmt(parg);
** 	parg->preci = preci;
** 	arg->pc = ft_strdup(parg->pc);
** 	parg->pc = str;
** 	DEBUG;
** }
*/

/*
** Convert parg in type, result in arg
*/

int			ft_ptype(t_prfarg *arg, t_prfarg *parg, t_ui type)
{
	DEBUGPTR(arg);
	DEBUGPTR(parg);
	DEBUGNBR(type);
	if (type & FL_CONV_DI)
		ft_ptype_di(arg, parg, type);
	else if (type & FL_CONV_S)
		ft_ptype_s(arg, parg, type);
	else if (type & FL_CONV_C)
		ft_ptype_c(arg, parg, type);
	else if (type & FL_CONV_BOUX)
		ft_ptype_boux(arg, parg, type);
	else
	{
		ft_prf_exit(EXIT_FAILURE, "Error:ptype: Unrecognized convertion\n");
		return (1);
	}
	ft_memcpy((void *)&arg->u, (void *)&parg->u, sizeof(parg->u));
	ft_check_alloc(arg->pc);
	DEBUGSTR(arg->pc);
	DEBUGNBR(arg->size);
	return (0);
}
