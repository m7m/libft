/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_type_w.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:19:59 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:20:00 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_type_wchar_t(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.wc = va_arg(prf->ap, wchar_t);
	ft_check_alloc(arg->pc = ft_wctoa(arg->u.wc));
	if (!(arg->size = ft_strlen(arg->pc)))
		arg->size = 1;
	DEBUGSTR(arg->pc);
}

void	ft_type_pwchar_t(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.pv = va_arg(prf->ap, wchar_t *);
	if (!arg->u.pv)
	{
		if (arg->preci_arg == arg)
		{
			arg->preci_arg = NULL;
			arg->preci = 0;
		}
		if (arg->width_arg == arg)
		{
			arg->width_arg = NULL;
			arg->width = 0;
		}
		ft_check_alloc(arg->pc = ft_strdup(MSGNULL));
	}
	else
		ft_check_alloc(arg->pc = ft_pwctoa((wchar_t *)arg->u.pv));
	DEBUGSTR(arg->pc);
}
