/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_type_px.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:26:27 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:26:27 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_type_px(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	if (arg->type & FL_LEN_H)
		prf->pnb_write = va_arg(prf->ap, char *);
	else if (arg->type & FL_LEN_HH)
		prf->pnb_write = va_arg(prf->ap, short *);
	else if (arg->type & FL_LEN_L)
		prf->pnb_write = va_arg(prf->ap, long *);
	else if (arg->type & FL_LEN_LL)
		prf->pnb_write = va_arg(prf->ap, long long *);
	else if (arg->type & FL_LEN_J)
		prf->pnb_write = va_arg(prf->ap, intmax_t *);
	else if (arg->type & FL_LEN_Z)
		prf->pnb_write = va_arg(prf->ap, size_t *);
	else if (arg->type & FL_LEN_T)
		prf->pnb_write = va_arg(prf->ap, ptrdiff_t *);
	else
		prf->pnb_write = va_arg(prf->ap, int *);
	DEBUGSTR(arg->pc);
}
