/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ptype_int.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:27:36 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:27:36 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Convert parg in type, result in arg
*/

int	ft_ptype_int(t_prfarg *parg)
{
	DEBUGPTR(parg);
	DEBUGNBR(parg->conv_flags);
	if (parg->conv_flags & FL_CONV_DI)
		return (parg->u.i);
	else if (parg->conv_flags & FL_CONV_S)
	{
		DEBUG;
		if (!ft_strcmp(MSGNULLPV, (char *)parg->u.pv))
			return (0);
		DEBUGNBR(parg->u.i);
		return (parg->u.i);
	}
	else if (parg->conv_flags & FL_CONV_C)
		return (parg->u.i);
	else if (parg->conv_flags & FL_CONV_BOUX)
		return (parg->u.i);
	else if (parg->conv_flags & FL_CONV_DBL)
		return (6);
	else
	{
		DEBUG;
		return (parg->u.i);
	}
	return (0);
}
