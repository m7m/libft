/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_type_c.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:20:05 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:20:06 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_type_char(t_prf *prf, t_prfarg *arg)
{
	char	str[2];

	DEBUG;
	arg->u.i = va_arg(prf->ap, int);
	str[0] = arg->u.c;
	str[1] = 0;
	ft_check_alloc(arg->pc = (char *)ft_memdup(str, sizeof(char) * 2));
	arg->size = 1;
	DEBUGSTR(arg->pc);
}

void	ft_type_charshort(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.i = va_arg(prf->ap, int);
	ft_check_alloc(arg->pc = ft_ctoa_base((char)arg->u.i, arg->base));
	DEBUGSTR(arg->pc);
}

void	ft_type_uchar(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.i = va_arg(prf->ap, int);
	ft_check_alloc(arg->pc = ft_uctoa_base(arg->u.uc, arg->base));
	DEBUGSTR(arg->pc);
}

void	ft_type_pchar(t_prf *prf, t_prfarg *arg)
{
	DEBUGNBR(arg->width);
	arg->u.pv = va_arg(prf->ap, void *);
	if (!arg->u.pv)
	{
		if (arg->preci_arg == arg)
		{
			arg->preci_arg = NULL;
			arg->preci = 0;
		}
		if (arg->width_arg == arg)
		{
			arg->width_arg = NULL;
			arg->width = 0;
		}
		arg->u.pv = MSGNULL;
	}
	ft_check_alloc(arg->pc = ft_strdup((char *)arg->u.pv));
	DEBUGSTR(arg->pc);
}
