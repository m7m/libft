/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_type_p.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:20:25 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:20:25 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_type_pvoid(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.pv = va_arg(prf->ap, void *);
	if (!arg->u.pv)
	{
		if (LINUX)
			ft_check_alloc(arg->pc = ft_strdup(MSGNULLPV));
		else
		{
			ft_check_alloc(arg->pc = ft_strdup("0"));
			arg->attr_flags |= FL_CO;
			arg->base = "0123456789abcdef";
			arg->conv_c = 'x';
		}
	}
	else
	{
		ft_check_alloc(arg->pc = ft_ultoa_base(arg->u.ul, arg->base));
		arg->attr_flags |= FL_CO;
		arg->base = "0123456789abcdef";
		arg->conv_c = 'x';
	}
	DEBUGSTR(arg->pc);
}
