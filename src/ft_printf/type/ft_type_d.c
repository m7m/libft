/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_type_d.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:28:11 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:28:11 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	ft_type_double__co(t_prfarg *arg)
{
	arg->attr_flags |= FL_CO;
	if (arg->minmaj)
		arg->conv_c = 'X';
	else
		arg->conv_c = 'x';
}

static void	ft_type_double__a(t_prfarg *arg)
{
	DEBUG;
	ft_check_alloc(arg->pc = ft_dtoa_base16(*(double *)&arg->u.l, arg->preci));
	if (*(arg->pc + 1) == 'n'
		|| *arg->pc == 'n'
		|| *(arg->pc + 1) == 'i'
		|| *arg->pc == 'i')
		return ;
	ft_type_double__co(arg);
	DEBUG;
}

static void	ft_type_double_fmt__type(t_prfarg *arg)
{
	DEBUG;
	if (arg->type & FL_CONV_AA)
		ft_type_double__a(arg);
	else if (arg->type & FL_CONV_EE)
		ft_check_alloc(arg->pc = ft_dtoa_scien(*(double *)&arg->u.l,
											arg->preci));
	else if (arg->type & FL_CONV_GG)
		ft_check_alloc(arg->pc = ft_dtoa_gg(*(double *)&arg->u.l, arg->preci));
	else
		ft_check_alloc(arg->pc = ft_dtoa(*(double *)&arg->u.l, arg->preci));
	DEBUGSTR(arg->pc);
}

void		ft_type_double_fmt(t_prfarg *arg)
{
	int	i;

	DEBUGNBR(arg->u.l);
	DEBUGNBR(arg->preci);
	if (!(arg->attr_flags & FL_PR) && !(arg->type & FL_CONV_AA))
		arg->preci = 6;
	else if (!(arg->attr_flags & FL_PR) && arg->type & FL_CONV_AA)
		arg->preci = -15;
	ft_type_double_fmt__type(arg);
	DEBUG;
	i = -1;
	if (arg->minmaj)
		while (arg->pc[++i])
			arg->pc[i] = ft_toupper(arg->pc[i]);
	else
		while (arg->pc[++i])
			arg->pc[i] = ft_tolower(arg->pc[i]);
	arg->attr_flags &= ~FL_PR;
	if (arg->preci_arg)
	{
		arg->conv_flags = arg->type;
		arg->type = FL_CONV_S;
	}
	DEBUGSTR(arg->pc);
}

void		ft_type_double(t_prf *prf, t_prfarg *arg)
{
	double	n;

	DEBUGNBR(arg->type);
	DEBUGNBR(arg->conv_flags);
	DEBUGNBR(arg->attr_flags);
	n = va_arg(prf->ap, double);
	arg->u.l = 0;
	arg->u.l = *(long *)&n;
	if (!arg->preci_arg)
		ft_type_double_fmt(arg);
	else
		arg->pc = "";
	DEBUGNBR(arg->type);
	DEBUGNBR(arg->conv_flags);
	DEBUGNBR(arg->attr_flags);
	DEBUGSTR(arg->pc);
}
