/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_type_si_usi_ptr_uptr.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:20:12 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:20:12 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_type_size_t(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.st = va_arg(prf->ap, size_t);
	ft_check_alloc(arg->pc = ft_ltoa_base(arg->u.l, arg->base));
	DEBUGSTR(arg->pc);
}

void	ft_type_ptrdiff_t(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.pd = va_arg(prf->ap, ptrdiff_t);
	ft_check_alloc(arg->pc = ft_ltoa_base(arg->u.l, arg->base));
	DEBUGSTR(arg->pc);
}

void	ft_type_usize_t(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	arg->u.ust = va_arg(prf->ap, ssize_t);
	ft_check_alloc(arg->pc = ft_ultoa_base(arg->u.ust, arg->base));
	DEBUGSTR(arg->pc);
}

void	ft_type_uptrdiff_t(t_prf *prf, t_prfarg *arg)
{
	arg->u.ul = va_arg(prf->ap, t_ul);
	ft_check_alloc(arg->pc = ft_ultoa_base(arg->u.ul, arg->base));
	DEBUGSTR(arg->pc);
}
