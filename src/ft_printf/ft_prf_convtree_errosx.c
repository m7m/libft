/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prf_convtree_errosx.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 20:57:18 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 20:57:19 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			ft_prf_convtree_errosx(t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	if (arg->attr_flags & FL_PR)
		arg->attr_flags &= ~FL_PR;
	if (arg->attr_flags & FL_OR)
		arg->attr_flags &= ~FL_OR;
	if (arg->ordre_ap)
	{
		arg->ordre_ap = -1;
		--prf->nbap;
	}
	ft_check_alloc(arg->pc = ft_strsub(prf->fmt, 0, 1));
	arg->size = 1;
	arg->type = FL_CONV_S;
	DEBUGSTR(prf->fmt);
	DEBUGSTR(arg->pc);
	return (1);
}
