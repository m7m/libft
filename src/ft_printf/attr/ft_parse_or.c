/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_or.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:18:28 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:18:38 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** $
*/

/*
** Ne traite que les chiffres de droite a gauche
*/

int	ft_strict_atoi_inv(char *str)
{
	int	i;
	int	j;

	DEBUGSTR(str);
	i = 0;
	j = 1;
	while (*str > 47 && *str < 58)
	{
		i += (*str - 48) * j;
		j *= 10;
		--str;
	}
	DEBUGNBR(i);
	return (i);
}

int	ft_prf_attr_or(t_prf *prf, t_prfarg *arg)
{
	DEBUGNBR(prf->nbap);
	DEBUGNBR(arg->width);
	if (LINUX && FL_OR & arg->attr_flags)
		return (RET_FLAGS);
	else if (!arg->width || arg->width != ft_strict_atoi_inv(prf->fmt - 1))
		return (0);
	arg->pc_arg = ft_lstarg_isexist(prf->lstarg, arg->width);
	if (arg->pc_arg)
		arg->attr_flags |= FL_OR;
	arg->ordre_ap = arg->width;
	arg->width = 0;
	if (prf->nbap < arg->ordre_ap)
		prf->nbap = arg->ordre_ap;
	DEBUGPTR(arg->pc_arg);
	DEBUGNBR(arg->ordre_ap);
	DEBUGNBR(prf->nbap);
	return (1);
}
