/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_sppl_mo_co_re.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:18:09 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:18:09 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** + or ' '
** return 2 pour %  +d
*/

int	ft_prf_attr_sppl(t_prf *prf, t_prfarg *arg)
{
	DEBUGC(*(prf->fmt));
	DEBUGNBR(arg->width);
	if (arg->width && LINUX)
		return (RET_FLAGS);
	DEBUG;
	if (*(prf->fmt) == ' ')
	{
		if (FL_SP & arg->attr_flags || arg->width)
			return (1);
		arg->attr_flags |= FL_SP;
	}
	else if (*(prf->fmt) == '+')
	{
		arg->attr_flags |= FL_PL;
	}
	return (1);
}

/*
** -
*/

int	ft_prf_attr_mo(t_prfarg *arg)
{
	DEBUG;
	if ((FL_MO & arg->attr_flags || arg->width) && LINUX)
		return (RET_FLAGS);
	arg->attr_flags |= FL_MO;
	return (1);
}

/*
** #
*/

int	ft_prf_attr_co(t_prfarg *arg)
{
	if ((FL_CO & arg->attr_flags || arg->width))
		return (RET_FLAGS);
	arg->attr_flags |= FL_CO;
	return (1);
}

/*
** 0
*/

int	ft_prf_attr_re(t_prfarg *arg)
{
	DEBUG;
	if (FL_RE & arg->attr_flags)
		return (RET_FLAGS);
	arg->attr_flags |= FL_RE;
	DEBUG;
	return (1);
}
