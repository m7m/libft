/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_mu.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:18:24 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 22:59:24 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** *
*/

/*
** Utilisation dans un autre attribut
*/

void		ft_prf_attr_mu_dol(t_prf *prf, t_prfarg *arg,
							t_prfarg **parg, int num_ap)
{
	DEBUG;
	*parg = ft_lstarg_isexist(prf->lstarg, num_ap);
	if (!*parg
		&& prf->nbap + 1 == num_ap
		&& !arg->ordre_ap)
	{
		DEBUG;
		*parg = arg;
		arg->ordre_ap = ++prf->nbap;
	}
	else if (!*parg)
	{
		DEBUG;
		DEBUGNBR(prf->nbap);
		*parg = ft_lstprf_newadd(prf->lstarg);
		(*parg)->type = FL_CONV_DI;
		(*parg)->attr_flags = FL_NONE;
		(*parg)->ordre_ap = num_ap;
		if (!arg->ordre_ap)
			arg->ordre_ap = ++prf->nbap;
	}
	if (prf->nbap_max < num_ap)
		prf->nbap_max = num_ap;
	DEBUGNBR(prf->nbap);
	DEBUGNBR(num_ap);
}

/*
** Utilisation dans un autre attribut
*/

static void	ft_prf_attr_mu_arg__end(t_prf *prf, t_prfarg *arg, int num_ap)
{
	t_prfarg	*argtmp;

	DEBUG;
	if ((argtmp = ft_lstarg_isexist(prf->lstarg, num_ap)))
	{
		DEBUG;
		arg->pc_arg = argtmp;
		arg->attr_flags |= FL_OR;
		arg->ordre_ap = 0;
	}
	if (prf->nbap_max < num_ap)
		prf->nbap_max = num_ap;
}

void		ft_prf_attr_mu_arg(t_prf *prf, t_prfarg *arg, t_prfarg **parg)
{
	int			num_ap;
	t_prfarg	*argtmp;

	DEBUG;
	if (arg->ordre_ap)
		num_ap = arg->ordre_ap++;
	else
		num_ap = ++prf->nbap;
	*parg = ft_lstprf_newadd(prf->lstarg);
	if ((argtmp = ft_lstarg_isexist(prf->lstarg, num_ap)))
	{
		DEBUG;
		(*parg)->pc_arg = argtmp;
	}
	else
		(*parg)->ordre_ap = num_ap;
	(*parg)->attr_flags = FL_NONE;
	(*parg)->type = FL_CONV_DI;
	++num_ap;
	arg->ordre_ap = 0;
	ft_prf_attr_mu_arg__end(prf, arg, num_ap);
	DEBUGPTR(*parg);
	DEBUGNBR(prf->nbap);
	DEBUGNBR(arg->ordre_ap);
	DEBUGNBR(num_ap);
}

int			ft_prf_attr_mu(t_prf *prf, t_prfarg *arg)
{
	int	i;
	int	nb_ap;

	DEBUG;
	if (LINUX && FL_MU & arg->attr_flags)
		return (RET_FLAGS);
	if (arg->width)
		arg->width = 0;
	DEBUG;
	i = 0;
	nb_ap = 0;
	while (ft_isdigit(*(prf->fmt + ++i)))
		nb_ap = *(prf->fmt + i) - 48 + (nb_ap * 10);
	if (*(prf->fmt + i) == '$')
	{
		if (nb_ap)
			ft_prf_attr_mu_dol(prf, arg, &arg->width_arg, nb_ap);
		prf->fmt += i;
	}
	else
		ft_prf_attr_mu_arg(prf, arg, &arg->width_arg);
	arg->attr_flags |= FL_MU;
	DEBUGNBR(arg->ordre_ap);
	return (1);
}
