/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_pr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:20:30 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 23:00:13 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** .
*/

static void	ft_prf_attr_pr__multi(int i, t_prf *prf, t_prfarg *arg)
{
	DEBUG;
	if (*(prf->fmt + i) == '$')
	{
		prf->fmt += i;
		if (arg->preci)
			ft_prf_attr_mu_dol(prf, arg, &arg->preci_arg, arg->preci);
	}
	else
	{
		ft_prf_attr_mu_arg(prf, arg, &arg->preci_arg);
		++prf->fmt;
	}
	arg->preci = 0;
}

int			ft_prf_attr_pr(t_prf *prf, t_prfarg *arg)
{
	int	multip;
	int	i;

	DEBUGC(*(prf->fmt));
	if (FL_PR & arg->attr_flags)
	{
		if (LINUX)
			return (RET_FLAGS);
		arg->preci = 0;
	}
	i = 0;
	arg->attr_flags |= FL_PR;
	multip = 0;
	if (*(prf->fmt + 1) == '*')
	{
		i = 1;
		multip = 1;
	}
	while (ft_isdigit(*(prf->fmt + ++i)))
		arg->preci = *(prf->fmt + i) - 48 + (arg->preci * 10);
	if (multip)
		ft_prf_attr_pr__multi(i, prf, arg);
	else
		prf->fmt += i - 1;
	return (1);
}
