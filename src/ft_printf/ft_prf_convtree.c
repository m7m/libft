/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prf_convtree.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 20:57:35 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/29 23:48:33 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_prf_convtree2(t_prf *prf, t_prfarg *arg)
{
	char	cfmt;

	cfmt = *prf->fmt;
	if (cfmt == 'e' || cfmt == 'E')
		ft_prf_conv_ee(arg);
	else if (cfmt == 'f' || cfmt == 'F')
		ft_prf_conv_ff(arg);
	else if (cfmt == 'g' || cfmt == 'G')
		ft_prf_conv_gg(arg);
	else if (cfmt == 'a' || cfmt == 'A')
		ft_prf_conv_aa(arg);
	else if (cfmt == 'n')
		ft_prf_conv_n(arg);
	else if (cfmt == 'P')
		ft_prf_conv_fd(arg);
	else
		return (0);
	return (1);
}

static int	ft_prf_convtree_next(t_prf *prf, t_prfarg *arg, int ret, char cfmt)
{
	if (!ret && cfmt)
		return (FT_CONVERR(prf, arg));
	if (!cfmt)
		return (0);
	if (!arg->ordre_ap)
		arg->ordre_ap = ++prf->nbap;
	return (1);
}

int			ft_prf_convtree(t_prf *prf, t_prfarg *arg)
{
	int		ret;
	char	cfmt;

	cfmt = *prf->fmt;
	ret = 0;
	arg->conv_c = cfmt;
	if (cfmt == 'd' || cfmt == 'D' || cfmt == 'i')
		ret = ft_prf_conv_di(arg);
	else if (cfmt == 's' || cfmt == 'S')
		ret = ft_prf_conv_s(arg);
	else if (cfmt == 'c' || cfmt == 'C')
		ret = ft_prf_conv_c(arg);
	else if (cfmt == 'p')
		ret = ft_prf_conv_p(arg);
	else if (cfmt == 'b' || cfmt == 'o' || cfmt == 'u' || cfmt == 'x'
			|| cfmt == 'B' || cfmt == 'O' || cfmt == 'U' || cfmt == 'X')
		ret = ft_prf_conv_ouxb(arg);
	else
		ret = ft_prf_convtree2(prf, arg);
	return (ft_prf_convtree_next(prf, arg, ret, cfmt));
}
