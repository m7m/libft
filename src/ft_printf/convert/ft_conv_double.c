/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conv_double.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:29:12 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:29:12 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_prf_conv_aa(t_prfarg *arg)
{
	DEBUG;
	arg->type |= FL_CONV_AA;
	if (ft_isupper(arg->conv_c))
		arg->minmaj = 1;
	return (1);
}

int	ft_prf_conv_ee(t_prfarg *arg)
{
	DEBUG;
	arg->type |= FL_CONV_EE;
	if (ft_isupper(arg->conv_c))
		arg->minmaj = 1;
	return (1);
}

int	ft_prf_conv_ff(t_prfarg *arg)
{
	DEBUG;
	arg->type |= FL_CONV_FF;
	if (ft_isupper(arg->conv_c))
		arg->minmaj = 1;
	return (1);
}

int	ft_prf_conv_gg(t_prfarg *arg)
{
	DEBUG;
	arg->type |= FL_CONV_GG;
	if (ft_isupper(arg->conv_c))
		arg->minmaj = 1;
	return (1);
}
