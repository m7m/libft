/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conv_pnm_pour.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:28:58 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/29 23:54:15 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_prf_conv_p(t_prfarg *arg)
{
	DEBUG;
	arg->base = "0123456789abcdef";
	arg->type |= FL_CONV_P;
	return (1);
}

/*
** Nb write in arg
*/

int	ft_prf_conv_n(t_prfarg *arg)
{
	DEBUG;
	arg->type |= FL_CONV_N;
	arg->attr_flags = FL_NONE;
	arg->width = 0;
	return (1);
}
