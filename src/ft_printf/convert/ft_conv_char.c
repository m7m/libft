/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conv_char.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:29:07 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:29:07 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_prf_conv_c(t_prfarg *arg)
{
	DEBUGNBR(arg->type);
	if (arg->conv_c == 'C')
		arg->type = FL_LEN_L;
	arg->type |= FL_CONV_C;
	arg->base = "0123456789";
	return (1);
}

int	ft_prf_conv_s(t_prfarg *arg)
{
	DEBUGNBR(arg->type);
	if (arg->conv_c == 'S')
		arg->type = FL_LEN_L;
	arg->type |= FL_CONV_S;
	DEBUGNBR(arg->width);
	return (1);
}
