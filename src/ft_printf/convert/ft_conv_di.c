/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conv_di.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:28:52 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:28:53 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_prf_conv_di(t_prfarg *arg)
{
	DEBUG;
	if (arg->conv_c == 'D')
		arg->type = FL_LEN_L;
	arg->type |= FL_CONV_DI;
	arg->base = "0123456789";
	DEBUGNBR(arg->type);
	return (1);
}
