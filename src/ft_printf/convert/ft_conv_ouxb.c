/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conv_ouxb.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:29:02 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:29:03 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_prf_conv_ouxb(t_prfarg *arg)
{
	DEBUGC(arg->conv_c);
	if (arg->conv_c != 'X' && ft_isupper(arg->conv_c))
		arg->type = FL_LEN_L;
	arg->type |= FL_CONV_BOUX;
	if (arg->conv_c == 'o' || arg->conv_c == 'O')
		arg->base = "01234567";
	else if (arg->conv_c == 'x')
		arg->base = "0123456789abcdef";
	else if (arg->conv_c == 'X')
		arg->base = "0123456789ABCDEF";
	else if (arg->conv_c == 'b' || arg->conv_c == 'B')
		arg->base = "01";
	else
		arg->base = "0123456789";
	DEBUGSTR(arg->base);
	DEBUGNBR(arg->ordre_ap);
	return (1);
}
