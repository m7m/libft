/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_conv_fd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/13 18:50:10 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/13 18:50:10 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** File descriptor
*/

int	ft_prf_conv_fd(t_prfarg *arg)
{
	DEBUG;
	arg->type |= FL_CONV_FD;
	arg->attr_flags = FL_NONE;
	return (1);
}
