/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_parse_attr.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 20:57:28 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 20:57:28 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_prfarg	*ft_prf_parse_attchr(t_prf *prf)
{
	t_prfarg	*arg;

	DEBUG;
	arg = ft_lstprf_newadd(prf->lstarg);
	++prf->fmt;
	if (FT_ATTTREE(prf, arg))
		return (arg);
	else
	{
		DEBUG;
		return (NULL);
	}
	return (NULL);
}
