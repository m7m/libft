/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_freeexit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/15 11:23:05 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/15 11:23:05 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "t_exit.h"

/*
** Enregistre une structure contenant:
** f = pointeur de fonction
** data = donnees envoie a f lors du free
** del_add si !0 supprime l'enregistrement
*/

static t_sttexit
	**ft_sttexit__static(void)
{
	static t_sttexit	*sttexit[MAXLISTFREE] = {NULL};

	return (sttexit);
}

void
	ft_sttexit(void *texit)
{
	t_sttexit	**sttexit;
	int			i;

	DEBUG;
	if (texit)
	{
		i = -1;
		sttexit = ft_sttexit__static();
		while (++i < MAXLISTFREE && sttexit[i])
			;
		if (i < MAXLISTFREE)
			sttexit[i] = texit;
	}
}

/*
** Supprime via un t_sttexit
*/

void
	ft_sttexit_del(t_sttexit *texit)
{
	t_sttexit	**sttexit;
	int			i;

	DEBUG;
	if (texit)
	{
		i = -1;
		sttexit = ft_sttexit__static();
		while (++i < MAXLISTFREE && sttexit[i] && sttexit[i] != texit)
			;
		if (i < MAXLISTFREE)
		{
			free(sttexit[i]);
			sttexit[i] = NULL;
		}
		while (i + 1 < MAXLISTFREE && sttexit[i + 1])
		{
			sttexit[i] = sttexit[i + 1];
			sttexit[i + 1] = NULL;
			++i;
		}
	}
}

/*
** Supprime en fonctione de data si est dans un des sttexit
*/

void
	ft_sttexit_deldata(void *data)
{
	t_sttexit	**sttexit;
	int			i;

	DEBUG;
	if (data)
	{
		i = -1;
		sttexit = ft_sttexit__static();
		while (++i < MAXLISTFREE && sttexit[i]
			&& sttexit[i]->data != data)
			;
		if (i < MAXLISTFREE && sttexit[i])
			ft_sttexit_del(sttexit[i]);
	}
}

void
	ft_sttexit_cal(void)
{
	t_sttexit	**sttexit;
	int			i;

	DEBUG;
	i = -1;
	sttexit = ft_sttexit__static();
	while (++i < MAXLISTFREE && sttexit[i])
	{
		sttexit[i]->f(sttexit[i]->data);
		free(sttexit[i]);
		sttexit[i] = NULL;
	}
}
