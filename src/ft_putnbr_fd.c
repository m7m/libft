/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 20:07:27 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/17 23:45:48 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	unsigned char	cn;

	if (n > 9)
	{
		ft_putnbr_fd(n / 10, fd);
		cn = n - (n / 10 * 10) + 48;
		write(fd, &cn, 1);
	}
	else if (n < 0)
	{
		write(fd, "-", 1);
		if (n < -9)
			ft_putnbr_fd((0 - (unsigned int)n) / 10, fd);
		cn = ((n - (n / 10 * 10)) * -1) + 48;
		write(fd, &cn, 1);
	}
	else
	{
		cn = n + 48;
		write(fd, &cn, 1);
	}
}
