/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 07:54:44 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/17 23:34:33 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	int		lens1;
	int		lens2;
	char	*tmp;
	char	*s1s2;

	lens1 = ft_strlen(s1);
	lens2 = ft_strlen(s2);
	if (!(s1s2 = (char *)malloc(sizeof(char) * (lens1 + lens2 + 1))))
		return (NULL);
	if (s1s2)
	{
		ft_strcpy(s1s2, s1);
		tmp = s1s2 + lens1;
		ft_strcpy(tmp, s2);
		s1s2[lens1 + lens2] = 0;
	}
	return (s1s2);
}
