/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultoa_base_s.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/28 19:56:37 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/28 19:56:37 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Convertie n en base
*/

int	ft_ultoa_base_s(t_ul n, char *base, char *dst)
{
	t_ul	nbase;
	t_ul	len;
	t_ul	i;

	i = -1;
	if ((nbase = (t_ul)ft_strlen(base)) > 1 && !ft_strocu(base))
	{
		len = 1;
		i = n;
		while (i /= nbase)
			++len;
		i = len;
		dst[len] = 0;
		while (len)
		{
			dst[--len] = base[n % nbase];
			n /= nbase;
		}
	}
	return (i);
}
