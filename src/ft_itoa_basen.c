/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_basen.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:03:33 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:03:35 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Convertie n en base
*/

static int	ft_itoa_basen__len(int n, int nbase)
{
	int len;

	len = 1;
	while (n /= nbase)
		++len;
	return (len);
}

char		*ft_itoa_basen(int n, int nbase)
{
	int		i;
	int		len;
	char	*base;
	char	*str;

	if (nbase < 2)
		return (NULL);
	base = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	len = ft_itoa_basen__len(n, nbase);
	if ((str = (char *)malloc(sizeof(char) * (len + 2))))
	{
		if ((i = (n < 0)))
		{
			str[0] = '-';
			str[len] = base[-(n % nbase)];
			n = -(n / nbase);
		}
		str[len + i] = 0;
		--i;
		while (--len > i && (str[len] = base[n % nbase]))
			n /= nbase;
	}
	return (str);
}

char		*ft_uitoa_basen(t_ui n, t_ui nbase)
{
	t_ui	len;
	t_ui	i;
	char	*str;
	char	*base;

	str = NULL;
	if (nbase > 1)
	{
		base = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		len = 1;
		i = n;
		while (i /= nbase)
			++len;
		if ((str = (char *)malloc(sizeof(char) * (len + 1))))
		{
			str[len] = 0;
			while (len)
			{
				str[--len] = base[n % nbase];
				n /= nbase;
			}
		}
	}
	return (str);
}
