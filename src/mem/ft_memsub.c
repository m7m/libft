/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 10:10:15 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 10:10:15 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Substitut dans 's' à partir de 'start' de taille 'len'
*/

void	*ft_memsub(void const *s, unsigned int start, size_t len)
{
	void	*tronc;

	tronc = (void *)malloc(sizeof(void) * (len + 1));
	if (tronc)
	{
		ft_memcpy(tronc, s + start, len);
		((char *)tronc)[len] = 0;
	}
	return (tronc);
}
