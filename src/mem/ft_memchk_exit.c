/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchk_exit.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/17 13:29:28 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/17 13:29:28 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_memchk_exit(void *ptr)
{
	if (!ptr)
		ft_exit(EXIT_FAILURE, MSG_OUT_OF_MEMORY);
}
