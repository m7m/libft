/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 09:12:51 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/18 06:08:27 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	size_t i;
	size_t t;

	i = 0;
	t = n / sizeof(long);
	while (i < t)
	{
		*((unsigned long *)dst + i) = *((unsigned long *)src + i);
		++i;
	}
	i = i * sizeof(long);
	while (i < n)
	{
		*((unsigned char *)dst + i) = *((unsigned char *)src + i);
		++i;
	}
	return (dst);
}
