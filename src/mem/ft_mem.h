/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mem.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/18 12:04:36 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/18 12:04:36 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MEM_H
# define FT_MEM_H

# include <stdlib.h>

void	*ft_memalloc(size_t size);
void	*ft_memallocset(size_t size, int c);
void	ft_memdel(void **ap);
void	*ft_memset(void *b, int c, size_t len);
void	*ft_memcpy(void *dst, const void *src, size_t n);
void	*ft_memccpy(void *dst, const void *src, int c, size_t n);
void	*ft_memmove(void *dst, const void *src, size_t len);
void	*ft_memchr(const void *s, int c, size_t n);
int		ft_memcmp(const void *s1, const void *s2, size_t n);
void	ft_bzero(void *s, size_t n);
void	*ft_memdup(void *s, size_t n);
void	*ft_memdup_resize(void *ptr, size_t size, size_t psize);

void	ft_chkalloc(void *ptr);
void	ft_memchk_exit(void *ptr);

void	**ft_malloc_array(size_t ty_l, size_t nb_l,
				size_t ty_c, size_t ta_col);
void	**ft_memalloc_array(size_t type, size_t slign, size_t scol);

void	*ft_memsub(void const *s, unsigned int start, size_t len);

/*
** bug
*/

void	**ft_memalloc_array_clr(size_t type, size_t slign, size_t scol);

#endif
