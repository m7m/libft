/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:11:08 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:11:09 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memdup(void *s, size_t n)
{
	void	*dus;

	if ((dus = (void *)malloc(n)))
		ft_memcpy(dus, s, n);
	return (dus);
}
