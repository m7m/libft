/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 12:11:54 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/20 22:31:10 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	int s1s2;

	if (n)
		while (--n)
		{
			if ((s1s2 = *(unsigned char *)s1 - *(unsigned char *)s2))
				return (s1s2);
			++s1;
			++s2;
		}
	if ((s1s2 = *(unsigned char *)s1 - *(unsigned char *)s2))
		return (s1s2);
	return (0);
}
