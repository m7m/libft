/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 11:52:45 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/04 22:25:01 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Rechercher un caractere dans une zone memoire.
*/

#include <string.h>

void	*ft_memchr(const void *s, int c, size_t n)
{
	while (n--)
		if (*(unsigned char *)s == (unsigned char)c)
			return ((void *)s);
		else
			++s;
	return (NULL);
}
