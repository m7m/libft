/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdup_resize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/20 18:53:54 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/20 18:58:28 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_mem.h"

/*
** Alloue un ptr de size + psize et copie ptr dedans
** puis retourne la nouvelle allocation
*/

void	*ft_memdup_resize(void *ptr, size_t size, size_t psize)
{
	void	*n;

	if ((n = malloc(size + psize)))
		ft_memcpy(n, ptr, size);
	return (n);
}
