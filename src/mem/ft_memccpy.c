/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 09:32:56 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/17 23:35:15 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t i;

	i = 0;
	while (i < n)
		if (((unsigned char *)src)[i] != (unsigned char)c)
			((unsigned char *)dst++)[0] = ((unsigned char *)src)[i++];
		else
		{
			((unsigned char *)dst)[0] = ((unsigned char *)src)[i];
			return (++dst);
		}
	return (NULL);
}
