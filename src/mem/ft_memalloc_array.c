/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc_array.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/18 01:17:28 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/30 04:56:18 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Alloue array a 1 dimenssion
*/

void	**ft_memalloc_array(size_t type, size_t slign, size_t scol)
{
	void	**list;
	void	*col;
	size_t	i;

	list = (void **)malloc(sizeof(void *) * slign + type * scol * slign);
	col = (void *)(list + slign + 1);
	i = 0;
	while (i < slign)
	{
		list[i] = col + type * scol * i;
		++i;
	}
	return (list);
}
