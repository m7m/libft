/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 07:46:53 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/28 01:40:55 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memset(void *b, int c, size_t len)
{
	if (len)
	{
		while (--len)
			((unsigned char*)b)[len] = (unsigned char)c;
		*(unsigned char*)b = (unsigned char)c;
	}
	return (b);
}
