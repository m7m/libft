/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vtexit.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/18 01:21:19 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/27 01:26:01 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "t_exit.h"

/*
** Sauvegarde ptrfree dans une liste
** si ptrfree et null alors free la liste
*/

static void
	**ft_vtexit__static(void)
{
	static void	*vall[MAXLISTFREE + 1] = {NULL};

	return (vall);
}

void
	ft_vtexit_cal(void)
{
	int		i;
	void	**vall;

	DEBUG;
	i = 0;
	vall = ft_vtexit__static();
	while (i < MAXLISTFREE && vall[i])
	{
		free(vall[i]);
		vall[i] = NULL;
		++i;
	}
}

void
	ft_vtexit_del(void *ptr)
{
	int		i;
	void	**vall;

	DEBUG;
	if (ptr)
	{
		i = 0;
		vall = ft_vtexit__static();
		while (i < MAXLISTFREE && vall[i] && vall[i] != ptr)
			++i;
		if (i >= MAXLISTFREE)
			return ;
		if (vall[i])
			vall[i] = NULL;
		while (i + 1 < MAXLISTFREE && vall[i + 1])
		{
			vall[i] = vall[i + 1];
			vall[i + 1] = NULL;
			++i;
		}
	}
}

void
	ft_vtexit(void *ptr)
{
	void	**vall;
	int		i;

	DEBUG;
	if (ptr)
	{
		i = 0;
		vall = ft_vtexit__static();
		while (i < MAXLISTFREE && vall[i])
			++i;
		if (i >= MAXLISTFREE)
			ft_exit(EXIT_FAILURE, "overflow MAXLISTFREE");
		DEBUGPTR(vall[i]);
		DEBUGPTR(ptr);
		vall[i] = ptr;
	}
}
