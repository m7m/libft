/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_s.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:15:34 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/20 14:37:16 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Convertie n en string dans str
** Return length write characters in str
*/

int	ft_itoa_s(int n, char *str)
{
	int		len;
	int		i;
	int		ret;

	i = n;
	len = 1;
	while (i /= 10)
		++len;
	if ((i = (n < 0)))
	{
		str[0] = '-';
		str[len] = -(n % 10) + 48;
		n = -(n / 10);
	}
	ret = len + i;
	str[len + i] = 0;
	--i;
	while (--len > i)
	{
		str[len] = n % 10 + 48;
		n /= 10;
	}
	return (ret);
}
