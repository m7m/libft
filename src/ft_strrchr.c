/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 00:12:26 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/04 22:09:02 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

/*
** Renvoie un pointeur sur la dernière occurrence
**  du caractère c dans la chaîne s.
*/

char	*ft_strrchr(const char *s, int c)
{
	char *ret;

	ret = NULL;
	while (*s)
		if (*s == c)
			ret = (char *)s++;
		else
			s++;
	if (c)
		return (ret);
	return ((char *)s);
}
