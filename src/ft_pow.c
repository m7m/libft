/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pow.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:09:13 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:09:13 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Fonction de puissance
** x élevé à la puissance y.
*/

double	ft_pow(double x, double y)
{
	double	p;

	p = 1;
	if (y >= 0)
	{
		++y;
		while (--y)
			p *= x;
	}
	else
	{
		--y;
		while (++y)
			p /= x;
	}
	return (p);
}
