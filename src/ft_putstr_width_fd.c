/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_width_fd.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/09 21:00:39 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/09 21:00:39 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Affiche n carractere sur fd
** si str est plus petit que n: complete par des espaces
** si str est plus grand que n: tronque str
*/

void	ft_putstr_width_fd(const char *str, int n, int fd)
{
	int		len;
	char	buff[4096];

	len = ft_strlen(str);
	if (n > 4095)
		return ;
	if (len > n - 2)
	{
		write(fd, str, n - 2);
		write(fd, "..", 2);
	}
	else
	{
		write(fd, str, len);
		n -= len;
		ft_memset(buff, ' ', n);
		write(fd, buff, n);
	}
}

void	ft_putnbr_width_fd(int nb, int n, int fd)
{
	char	str[255];

	ft_itoa_s(nb, str);
	ft_putstr_width_fd(str, n, fd);
}
