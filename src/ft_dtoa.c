/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dtoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:51:30 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:51:31 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Retoune l'exposant de n
*/

int		ft_d_ret_expo(double n)
{
	int	expo;

	if (n < 0)
		n = -n;
	if (n > 0X1P+64)
		expo = 310;
	else if (n >= 10.)
		expo = 64;
	else
		expo = 0;
	DEBUGNBR(expo);
	if (n >= 10.)
		while (!(int)(n / ft_pow(10., (double)--expo)))
			;
	else if (n < 1. && n)
	{
		while (!(int)(n *= 10))
			--expo;
		--expo;
	}
	DEBUGNBRUL(n);
	DEBUGNBR(expo);
	return (expo);
}

/*
** Convert double to string
*/

char	*ft_dtoa(double n, int preci)
{
	int		neg;
	int		expo;
	char	*str;

	if (ft_isnan(&str, n))
		return (str);
	DEBUG;
	neg = (n < 0);
	expo = ft_d_ret_expo(n);
	str = NULL;
	if (preci >= 0 &&
		((expo >= 0 &&
		(str = (char *)ft_strnew(sizeof(char) * (preci + neg + expo + 3))))
		||
		(expo < 0 &&
		(str = (char *)ft_strnew(sizeof(char) * (preci + neg + -expo + 3))))))
	{
		if (neg)
			str[0] = '-';
		ft_dto_str(n, str + neg, expo, preci);
	}
	DEBUGSTR(str);
	return (str);
}

/*
** #define MALLOC_CHECK_ 3
** #include <stdio.h>
** #include <float.h>
** int main()
** {
** 	char	*str;
** 	double	d;
** 	int		p;
** 	d = -6666.66666666666;
** 	p = 0;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = -5555.55555555555;
** 	p = 0;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = -1234.5555;
** 	p = 2;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 6666.66666666666;
** 	p = 15;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = -6666.66666666666;
** 	p = 5;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = -6666.66666666666;
** 	p = 4;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = -6666.66666666666;
** 	p = 3;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = -6666.66666666666;
** 	p = 2;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = -6666.66666666666;
** 	p = 1;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = -6666.66666666666;
** 	p = 6;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = -5555.55555555555;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = -1234.56012345678;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 0;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 1;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 2;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 0.000123456;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 12.3456;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 1444565444646.6465424242242;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = -1444565444646.6465424242242454654;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = -0.0000000000000000123456012345678;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 0.00000000000000000000000000000001;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 10000000000000000000000000000000.;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 1000000000000000000000000000000.;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 100000000000000000000000000000.;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 10000000000000000000000000000.;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 1000000000000000000000000000.;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 100000000000000000000000000.;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 10000000000000000000000000.;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 1000000000000000000000000.;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 100000000000000000000000.;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 10000000000000000000000.;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 1000000000000000000000.;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 100000000000000000000.;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	d = 9999.99999999;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);
**      fflush(stdout);free(str);
** 	return 0;
** 	d = DBL_MAX;
** 	p = 6;
** 	str = ft_dtoa(d, p);
** 	printf("%1$d  %3$.*2$f \n%1$d  %4$s\n", __LINE__, p, d, str);fflush(stdout);
** 	free(str);
** 	return (0);
** }
*/
