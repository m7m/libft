/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sizettoa_s.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/29 19:09:00 by mmichel           #+#    #+#             */
/*   Updated: 2016/10/29 19:09:00 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Convertie n en string dans str
*/

size_t	ft_sizettoa_s(size_t n, char *str)
{
	size_t	len;
	size_t	i;

	i = n;
	len = 1;
	while (i /= 10)
		++len;
	str[len] = 0;
	i = len;
	while (len)
	{
		--len;
		str[len] = n % 10 + 48;
		n /= 10;
	}
	return (i);
}
