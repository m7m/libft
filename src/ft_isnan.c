/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isnan.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:14:50 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:14:51 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Si n est NaN retoune 1
** -1 si erreur
*/

int	ft_isnan(char **nan, double n)
{
	int		neg;
	int		expo;
	t_ul	ln;

	if (nan)
	{
		*nan = NULL;
		ln = *(t_ul *)&n;
		neg = (int)(ln >> (sizeof(long) * 8 - 1));
		expo = (int)((ln >> (sizeof(long) * 8 - 12)) & 0x7FF);
		ln &= ~((t_ul)0xFFF << (sizeof(long) * 8 - 12));
		if (expo != 0x7FF)
			return (0);
		if (ln && neg && LINUX)
			*nan = ft_strdup("-nan");
		else if (ln)
			*nan = ft_strdup("nan");
		else if (neg && !ln)
			*nan = ft_strdup("-inf");
		else if (!ln)
			*nan = ft_strdup("inf");
		if (*nan)
			return (1);
	}
	return (-1);
}
