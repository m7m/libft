/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isint.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 04:32:55 by mmichel           #+#    #+#             */
/*   Updated: 2016/11/22 04:32:55 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

#define STRINGIFY2(x) #x
#define STRINGIFY(x) STRINGIFY2(x)

#include <limits.h>

#ifndef __x86_64__
# error Architecture non 64bits, modifier STR_INT_MIN
#endif
#ifndef STR_INT_MAX
# define STR_INT_MAX STRINGIFY(INT_MAX)
#endif
#ifndef STR_INT_MIN
# define STR_INT_MIN "-2147483648"
#endif

static int
	ft_isint__neg(char *s)
{
	size_t			len;
	static size_t	lenintmin = 0;

	DEBUG;
	if (!ft_strisdigit(s + 1))
		return (0);
	if (!lenintmin)
		lenintmin = ft_strlen(STR_INT_MIN);
	len = ft_strlen(s);
	DEBUGNBR(len);
	DEBUGNBR(lenintmin);
	if (len < lenintmin)
		return (1);
	DEBUGNBR(ft_strcmp(STR_INT_MIN, s));
	return (len == lenintmin && ft_strcmp(STR_INT_MIN, s) >= 0);
}

static int
	ft_isint__pos(char *s)
{
	size_t			len;
	static size_t	lenintmax = 0;

	DEBUG;
	if (!ft_strisdigit(s))
		return (0);
	if (!lenintmax)
		lenintmax = ft_strlen(STR_INT_MAX);
	len = ft_strlen(s);
	DEBUGNBR(len);
	DEBUGNBR(lenintmax);
	if (len < lenintmax)
		return (1);
	DEBUGNBR(ft_strcmp(STR_INT_MAX, s));
	return (len == lenintmax && ft_strcmp(STR_INT_MAX, s) >= 0);
}

int
	ft_isint(char *s)
{
	DEBUGSTR(s);
	if (*s == '-')
		return (ft_isint__neg(s));
	return (ft_isint__pos(s));
}
