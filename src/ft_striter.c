/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 08:55:16 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/02 13:37:57 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Applique la fonction f a chaque caractere de la chaine de caracteres
** passee en parametre. Chaque caractere est passe par adresse
** a la fonction f afin de pouvoir etre modifie si necessaire.
*/

void	ft_striter(char *s, void (*f)(char *))
{
	while (*s)
		f(s++);
}
