/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atexit.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/18 01:17:42 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/18 01:19:32 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Enregistre f dans la static si non null
** si f est null: appel list f et vide la liste static
*/

static void
	(**ft_atexit__static(void))(void)
{
	static void	(*fall[MAXLISTFREE + 1])(void) = {NULL};

	return (fall);
}

void
	ft_atexit_del(void (*f)(void))
{
	int		i;
	void	(**fall)(void);

	DEBUG;
	if (f)
	{
		i = 0;
		fall = ft_atexit__static();
		while (i < MAXLISTFREE && fall[i] && fall[i] != f)
			++i;
		if (i >= MAXLISTFREE)
			return ;
		if (fall[i])
			fall[i] = NULL;
		while (i + 1 < MAXLISTFREE && fall[i + 1])
		{
			fall[i] = fall[i + 1];
			fall[i + 1] = NULL;
			++i;
		}
	}
}

void
	ft_atexit_cal(void)
{
	int		i;
	void	(**fall)(void);
	void	(*call_fall)(void);

	DEBUG;
	i = MAXLISTFREE;
	fall = ft_atexit__static();
	while (i)
	{
		--i;
		DEBUGPTR(fall[i]);
		if (fall[i])
		{
			call_fall = fall[i];
			fall[i] = NULL;
			call_fall();
		}
	}
}

void
	ft_atexit(void (*f)(void))
{
	int		i;
	void	(**fall)(void);

	DEBUG;
	if (f)
	{
		i = 0;
		fall = ft_atexit__static();
		while (i < MAXLISTFREE && fall[i])
			++i;
		if (i >= MAXLISTFREE)
			ft_exit(EXIT_FAILURE, "overflow MAXLISTFREE");
		DEBUGPTR(fall[i]);
		DEBUGPTR(f);
		fall[i] = f;
	}
}
