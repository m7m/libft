/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dtoa_gg.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:02:04 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:52:13 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_debug.h"

static int	ft_dtoa__decal(double n)
{
	double	cpn;
	long	si;
	int		i;

	if (n < 0)
		n = -n;
	cpn = n;
	i = -1;
	si = (long)n;
	DEBUGNBRUL(si);
	while (si == (long)(cpn * ft_pow(10., ++i)))
	{
		n = (n - (int)n) * 10;
		si = si * 10 + (long)n;
	}
	DEBUGNBR(i);
	return (i + 1);
}

static char	*ft_dtoa__scien(double n, int preci)
{
	int		lendiff;
	int		expo;
	char	*str;
	char	*estr;

	DEBUG;
	if ((str = ft_dtoa_scien(n, preci - 1)))
	{
		estr = ft_strchr(str, 'e');
		lendiff = (estr - str) - 1;
		expo = lendiff;
		while (str[expo] == 48)
			--expo;
		if (expo != lendiff)
			ft_strcpy(str + expo, estr);
	}
	DEBUGSTR(str);
	return (str);
}

static char	*ft_dtoa__flo(double n, int preci, int expo, int len)
{
	char	*str;

	DEBUG;
	if (len < preci && expo <= 0)
		str = ft_dtoa(n, preci + expo - 1);
	else
		str = ft_dtoa(n, preci - expo - 1);
	if (str)
	{
		DEBUGSTR(str);
		len = ft_strlen(str);
		if (len > 1)
		{
			while (str[--len] == 48)
				;
		}
		if (str[len] == '.')
			str[len] = 0;
		else
			str[len + 1] = 0;
	}
	DEBUGSTR(str);
	return (str);
}

char		*ft_dtoa_gg(double n, int preci)
{
	int		expo;
	int		len;
	char	*str;

	if (ft_isnan(&str, n))
		return (str);
	if (preci < 0)
		preci = 6;
	else if (!preci)
		preci = 1;
	DEBUGNBR(n);
	expo = ft_d_ret_expo(n);
	DEBUGNBR(expo);
	DEBUGNBR(preci);
	if (n && (expo >= preci || expo < -4))
		return (ft_dtoa__scien(n, preci));
	if (expo >= 0)
		len = ft_dtoa__decal(n / ft_pow(10., (double)expo));
	else
		len = ft_dtoa__decal(n * ft_pow(10., (double)-expo));
	DEBUGNBR(len);
	return (ft_dtoa__flo(n, preci, expo, len));
}

/*
** #include <stdio.h>
** int main()
** {
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 1234567891234567890000.,
** 		   ft_dtoa_gg(1234567891234567890000., -1));fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 0.0000123456789,
** 		   ft_dtoa_gg(0.0000123456789, -1));fflush(stdout);
** 	printf("%d  %.0g \n%1$d  %s\n", __LINE__, 123.46, ft_dtoa_gg(123.46, 0));
** 	fflush(stdout);
** 	printf("%d  %.1g \n%1$d  %s\n", __LINE__, 123.46, ft_dtoa_gg(123.46, 1));
** 	fflush(stdout);
** 	printf("%d  %.0g \n%1$d  %s\n", __LINE__, 666.666666,
** 		   ft_dtoa_gg(666.666666, 0));fflush(stdout);
** 	printf("%d  %.0g \n%1$d  %s\n", __LINE__, 6.666666,
** 		   ft_dtoa_gg(6.666666, 0));fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 3., ft_dtoa_gg(3., -1));
** 	fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 4., ft_dtoa_gg(4., -1));
** 	fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 5., ft_dtoa_gg(5., -1));
** 	fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 15., ft_dtoa_gg(15., -1));
** 	fflush(stdout);
** 	printf("%d  %e \n%1$d  %s\n", __LINE__, 0.000000000123456,
** 		   ft_dtoa_scien(0.000000000123456, 6));fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 0.000000000123456,
** 		   ft_dtoa_gg(0.000000000123456, -1));fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 11., ft_dtoa_gg(11., -1));
** 	fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 123456., ft_dtoa_gg(123456., -1));
** 	fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 0.000000000000000100023456,
** 		     ft_dtoa_gg(0.000000000000000100023456, 6));fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 0.0000000000000001000023456
** 		   , ft_dtoa_gg(0.0000000000000001000023456, 6));fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 0.00000000000000010000023456,
** 		     ft_dtoa_gg(0.00000000000000010000023456, 6));fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 0.000000000000000100000023456,
** 		     ft_dtoa_gg(0.000000000000000100000023456, 6));fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 0.0000000000000001000000023456,
** 		     ft_dtoa_gg(0.0000000000000001000000023456, 6));fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 123456000.,
** 		   ft_dtoa_gg(123456000., 6));fflush(stdout);
** 	printf("%d  %.2g \n%1$d  %s\n", __LINE__, 123., ft_dtoa_gg(123., 2));
** 	fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 123., ft_dtoa_gg(123., 6));
** 	fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 1234., ft_dtoa_gg(1234., 6));
** 	fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 12345., ft_dtoa_gg(12345., 6));
** 	fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 123456., ft_dtoa_gg(123456., 6));
** 	fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 1234567., ft_dtoa_gg(1234567., 6));
** 	fflush(stdout);
** 	printf("%d  %.8g \n%1$d  %s\n", __LINE__, 1234567.,
** 		   ft_dtoa_gg(1234567., 8));fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 0.000123456,
** 		   ft_dtoa_gg(0.000123456, 6));fflush(stdout);
** 	printf("%d  %.3g \n%1$d  %s\n", __LINE__, 666.666666,
** 		   ft_dtoa_gg(666.666666, 3));fflush(stdout);
** 	printf("%d  %.4g \n%1$d  %s\n", __LINE__, 666.666666,
** 		   ft_dtoa_gg(666.666666, 4));fflush(stdout);
** 	printf("%d  %.5g \n%1$d  %s\n", __LINE__, 666.666666,
** 		   ft_dtoa_gg(666.666666, 5));fflush(stdout);
** 	printf("%d  %g \n%1$d  %s\n", __LINE__, 0.123456, ft_dtoa_gg(0.123456, 6));
** 	fflush(stdout);
** 	printf("%d  %.3g \n%1$d  %s\n", __LINE__, 123.456, ft_dtoa_gg(123.456, 3));
** 	fflush(stdout);
** 	printf("%d  %.4g \n%1$d  %s\n", __LINE__, 123.456, ft_dtoa_gg(123.456, 4));
** 	fflush(stdout);
** 	printf("%d  %.5g \n%1$d  %s\n", __LINE__, 123.456, ft_dtoa_gg(123.456, 5));
** 	fflush(stdout);
** 	printf("%d  %.15g \n%1$d  %s\n", __LINE__, -1234.56789123456,
**                          		    ft_dtoa_gg(-1234.56789123456, 15));
** 	fflush(stdout);
** 	printf("%d  %.15g\n%1$d  %s\n", __LINE__, -6666.66666666666,
** 		   ft_dtoa_gg(-6666.66666666666, 15));fflush(stdout);
** 	printf("%d  %.2g\n%1$d  %s\n", __LINE__, 0.,
** 		   ft_dtoa_gg(0., 2));fflush(stdout);
** 	printf("%d  %.1g\n%1$d  %s\n", __LINE__, 0.,
** 		   ft_dtoa_gg(0., 1));fflush(stdout);
** 	printf("%d  %.0g\n%1$d  %s\n", __LINE__, 0.,
** 		   ft_dtoa_gg(0., 0));fflush(stdout);
** 	printf("%d  %.2g\n%1$d  %s\n", __LINE__, 10.,
** 		   ft_dtoa_gg(10., 2));fflush(stdout);
** 	printf("%d  %.1g\n%1$d  %s\n", __LINE__, 10.,
** 		   ft_dtoa_gg(10., 1));fflush(stdout);
** 	printf("%d  %.0g\n%1$d  %s\n", __LINE__, 10.,
** 		   ft_dtoa_gg(10., 0));fflush(stdout);
** 	printf("%d  %.2g\n%1$d  %s\n", __LINE__, 5.,
** 		   ft_dtoa_gg(5., 2));fflush(stdout);
** 	printf("%d  %.1g\n%1$d  %s\n", __LINE__, 5.,
** 		   ft_dtoa_gg(5., 1));fflush(stdout);
** 	printf("%d  %.0g\n%1$d  %s\n", __LINE__, 5.,
** 		   ft_dtoa_gg(5., 0));fflush(stdout);
** 	return (0);
** }
*/
