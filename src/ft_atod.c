/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atod.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/27 06:23:34 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/27 06:26:57 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

static double	ft_atod_dot(double i, double p, char c)
{
	if (c == '.')
		return (i);
	return (i + (c - 48) / p);
}

double			ft_atod(const char *str)
{
	double	i;
	double	m;
	double	p;

	i = 0;
	m = 1;
	p = 0.1;
	while (*str > 0 && *str <= 32)
		++str;
	if (*str == '-')
		m = *str++ - 46;
	else if (*str == '+')
		m = 44 - *str++;
	while (*str)
	{
		if (p == 0.1 && *str > 47 && *str < 58)
			i = i * 10 + *str - 48;
		else if ((*str > 47 && *str < 58 && p >= 1) || (p < 1 && *str == '.'))
			i = ft_atod_dot(i, p *= 10, *str);
		else if (i || m)
			return (i * m);
		++str;
	}
	return (i * m);
}
