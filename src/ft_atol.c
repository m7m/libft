/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/16 04:32:26 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/27 06:27:58 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

long	ft_atol(const char *str)
{
	long i;
	long m;

	i = 0;
	m = 1;
	while (*str > 0 && *str <= 32)
		str = str + 1;
	if (*str == '-')
		m = *str++ - 46;
	else if (*str == '+')
		m = 44 - *str++;
	while (*str)
	{
		if (*str > 47 && *str < 58)
			i = i * 10 + *str - 48;
		else if (i || m)
			return (i * m);
		str++;
	}
	return (i * m);
}
