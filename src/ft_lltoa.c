/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lltoa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:09:50 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:09:55 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_lltoa(t_ll n)
{
	t_ll	i;
	t_ll	len;
	char	*str;

	i = n;
	len = 1;
	while (i /= 10)
		len += 1;
	if ((str = (char *)malloc(sizeof(char) * (len + 2))))
	{
		if ((i = (n < 0)))
		{
			str[0] = '-';
			str[len] = -(n % 10) + 48;
			n = -(n / 10);
		}
		str[len + i] = 0;
		--i;
		while (--len > i)
		{
			str[len] = n % 10 + 48;
			n /= 10;
		}
	}
	return (str);
}
