/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btoa_base16.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:13:36 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:44:13 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Retire les zero a gauche superflu
*/

static int	ft__clrzero_left(char *str, size_t len)
{
	DEBUGSTR(str);
	--len;
	while (len && str[len] == 48)
		--len;
	DEBUGNBR(len);
	if (len)
		ft_strncpy(str, str + 1, len);
	else
		str[0] = 0;
	DEBUGSTR(str);
	return (len);
}

static void	ft_dtoa__denorm(char *str, size_t len, int preci)
{
	DEBUGNBR(preci);
	if (len && preci)
	{
		ft_strncpy(str, "0.", 2);
		ft_strcpy(str + 2 + len, "P-1022");
	}
	else if (!preci && len && LINUX)
		ft_strcpy(str, "1P-1022");
	else if (!preci && len)
		ft_strcpy(str, "2P-1023");
	else
		ft_strncpy(str, "0P+0", 4);
	DEBUGSTR(str);
}

static void	ft_dtoa_base16_compl(char *str, size_t len, int expo, int preci)
{
	int	i;

	DEBUGSTR(str);
	if (!expo)
		return (ft_dtoa__denorm(str, len, preci));
	i = 1;
	if (!preci && len > 1)
		ft_strncpy(str, "2", i);
	else if (!preci)
		ft_strncpy(str, "1", i);
	else
		ft_strncpy(str, "1.", ++i);
	i += preci;
	str[i] = 'P';
	expo -= 1023;
	if (expo < 0)
		str[++i] = '-';
	else
		str[++i] = '+';
	if (expo < 0)
		expo = -expo;
	ft_uitoa_s(expo, str + ++i);
	DEBUGSTR(str);
}

/*
** Extrait les information du double depuis la mémoire
** "Ligne 11 : preci est en base 16 donc 4bit en base 2"
*/

static char	*ft_dtoa__hex(long ln, int expo_neg[2], int *len, int preci)
{
	char	*strdump;
	char	*str;

	strdump = NULL;
	str = NULL;
	expo_neg[1] = (ln >> (sizeof(long) * 8 - 1)) & 1;
	expo_neg[0] = (ln >> (sizeof(long) * 8 - 12)) & 0x7FF;
	ln &= ~(long)((long)0xFFF << (sizeof(long) * 8 - 12));
	if ((size_t)preci < sizeof(double) * 2 - 1
		&& ((ln >> ((sizeof(long) - preci - 1) * 8)) & (long)0xFF) > 0xF)
		ln += (long)0x1 << (63 - preci * 4 - 12);
	if (preci >= 0
		&& (strdump = ft_hexdump(&ln, sizeof(double) - 1))
		&& (str = ft_strnew(preci + sizeof(char) * (2 + 6 + 1))))
	{
		*len = ft__clrzero_left(strdump,
								(sizeof(double) - 1) * sizeof(char) * 2);
		ft_strncpy(str + 2 + expo_neg[1], strdump, preci);
		DEBUGSTR(strdump);
	}
	free(strdump);
	DEBUGNBR(expo_neg[0]);
	DEBUGNBR(expo_neg[1]);
	return (str);
}

/*
** Convertie n en base 16 avec indicateur exponentielle
** 0X1.mmmP+-e
*/

char		*ft_dtoa_base16(double n, int preci)
{
	int		expo_neg[2];
	int		len;
	int		nonepreci;
	char	*str;

	DEBUGNBR(n);
	str = NULL;
	if (ft_isnan(&str, n))
		return (str);
	nonepreci = (preci == -15);
	if (nonepreci)
		preci = 15;
	if ((str = ft_dtoa__hex(*(long *)&n, expo_neg, &len, preci)))
	{
		if (expo_neg[1])
			str[0] = '-';
		if (nonepreci && len < preci)
			preci = len;
		else if (!nonepreci && len < preci)
			while (preci > len)
				str[expo_neg[1] + ++len + 1] = 48;
		ft_dtoa_base16_compl(str + expo_neg[1], len, expo_neg[0], preci);
	}
	DEBUGSTR(str);
	return (str);
}

/*
** #include <stdio.h>
** int main(void)
** {
** 	char *str;
** 	double d = 0b0100011100011101011100001010001111010111000010100100;
** 	str = ft_dtoa_base16(d);
** 	printf("s:%s f:%f \na:%A\n", str, d, d);fflush(stdout);
** 	free(str);
** 	str = ft_dtoa_base16(d = 0);
** 	printf("s:%s f:%f \na:%A\n", str, d, d);fflush(stdout);
** 	free(str);
** 	str = ft_dtoa_base16(d = -0.0001);
** 	printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout);
** 	free(str);
** 	str = ft_dtoa_base16(d = 1);
** 	printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout);
** 	free(str);
** 	str = ft_dtoa_base16(d = 5);
** 	printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout);
** 	free(str);
** 	str = ft_dtoa_base16(d = 0x1.1F5P+0);
** 	printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout);
** 	free(str);
** 	str = ft_dtoa_base16(d = 2);
** 	printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout);
** 	free(str);
** 	str = ft_dtoa_base16(d = 3);
** 	printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout);
** 	free(str);
** 	str = ft_dtoa_base16(d = 0.0001);
** 	printf("s:%s f:%f \na:%A %e\n", str, d, d, d);fflush(stdout);
** 	free(str);
** 	return (0);
** }
*/
