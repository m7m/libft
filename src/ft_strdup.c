/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 12:43:09 by mmichel           #+#    #+#             */
/*   Updated: 2015/11/30 13:44:14 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Dupliquer une chaine
*/

#include "libft.h"

char	*ft_strdup(const char *s1)
{
	int		len;
	char	*dus1;

	len = ft_strlen(s1) + 1;
	dus1 = (char *)malloc(sizeof(char) * (len));
	if (dus1)
		ft_strncpy(dus1, s1, len);
	return (dus1);
}
