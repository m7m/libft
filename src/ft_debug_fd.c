/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_debug_fd.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/10 08:44:03 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/10 08:44:03 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_debug.h"

void
	ft_debug_common_fd(const char *fonc, int li, const char *fc, const char *n)
{
	int		fd;

	fd = ft_retfd();
	ft_putstr_width_fd(fonc, 30, fd);
	ft_putstr_fd(":\t", fd);
	ft_putnbr_width_fd(li, 5, fd);
	if (n)
	{
		ft_putstr_fd(":\t", fd);
		ft_putstr_width_fd(n, 20, fd);
		ft_putstr_fd(" = ", fd);
	}
	else
		ft_putstr_fd(fc, fd);
}

void
	ft_debugstr_fd(const char *str)
{
	int		fd;
	char	*ptr;
	char	dst[1024];

	fd = ft_retfd();
	ptr = (char *)str;
	if (str && *str)
		ft_putstr_width_fd(str, 20, fd);
	else
		ft_putstr_width_fd("[nil]", 20, fd);
	ft_ultoa_base_s((unsigned long)ptr, "0123456789ABCDEF", dst);
	ft_putstr_fd(" 0x", fd);
	ft_putstr_fd(dst, fd);
	ft_putchar_fd('\n', fd);
}

void
	ft_debugc_fd(const char c)
{
	int	fd;

	fd = ft_retfd();
	ft_putchar_fd(c, fd);
	ft_putchar_fd('\n', fd);
}

void
	ft_debugptr_fd(void *ptr)
{
	int		fd;
	char	str[1024];

	fd = ft_retfd();
	ft_ltoa_base_s((long)ptr, "0123456789ABCDEF", str);
	ft_putstr_width_fd("[ptr]", 21, fd);
	ft_putstr_fd("0x", fd);
	ft_putstr_fd(str, fd);
	ft_putchar_fd('\n', fd);
}
