/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_buff__.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/13 23:35:23 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/13 23:35:23 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Fonction interne pour ft_buff.c
*/

void
	*ft_buff___static(void)
{
	static void	*buff[MAXFD + 2] = {NULL};

	return (buff);
}

void
	*ft_buff___stock(int fd)
{
	void	**buff;

	DEBUG;
	if (fd < 0)
		ft_exit(EXIT_FAILURE, "Error: fd invalide");
	else if (fd >= MAXFD)
		ft_exit(EXIT_FAILURE, "Error: fd > MAXFD");
	buff = ft_buff___static();
	if (!buff[fd])
	{
		if (!(buff[fd] = malloc(sizeof(char) * (BUFF_SIZE + 1)
								+ sizeof(size_t))))
			ft_exit(EXIT_FAILURE, MSG_OUT_OF_MEMORY);
		*((size_t *)(buff[fd])) = 0;
		buff[MAXFD] = (void *)0x1;
	}
	return (buff[fd]);
}

size_t
	*ft_buff___size_buff(void **buff)
{
	size_t	*buff_size;

	DEBUGPTR(*buff);
	buff_size = ((size_t *)(*buff));
	*buff = (void *)(buff_size + 1);
	return (buff_size);
}
