/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 03:46:50 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:11:01 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_itoa(int n)
{
	int		len;
	int		i;
	char	*str;

	i = n;
	len = 1;
	while (i /= 10)
		++len;
	if ((str = (char *)malloc(sizeof(char) * (len + 2))))
	{
		if ((i = (n < 0)))
		{
			str[0] = '-';
			str[len] = -(n % 10) + 48;
			n = -(n / 10);
		}
		str[len + i] = 0;
		--i;
		while (--len > i)
		{
			str[len] = n % 10 + 48;
			n /= 10;
		}
	}
	return (str);
}
