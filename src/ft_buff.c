/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_buff.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/13 20:59:30 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/13 20:59:30 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_buff.h"
#include "ft_buff__.h"

/*
** Le buffer contien des donnees sur lui meme
**  et non juste les donnees envoie sur ft_buff()
*/

/*
** Force l'ecriture de tout les buffs et free les buffs
*/

void
	ft_fflush_all(void)
{
	void	**buff;
	void	*tmp;
	size_t	*buff_size;
	int		fd;

	DEBUG;
	fd = -1;
	buff = ft_buff___static();
	if (buff[MAXFD])
	{
		while (++fd < MAXFD)
			if (buff[fd])
			{
				DEBUGNBR(fd);
				tmp = buff[fd];
				buff_size = ft_buff___size_buff(&tmp);
				if (*buff_size)
					write(fd, tmp, *buff_size);
				free(buff[fd]);
				buff[fd] = NULL;
			}
	}
	buff[MAXFD] = NULL;
}

/*
** Force l'ecriture du buff associe au fd
*/

void
	ft_fflush(int fd)
{
	void	*buff;
	size_t	*buff_size;

	DEBUG;
	buff = ft_buff___stock(fd);
	if (buff)
	{
		buff_size = ft_buff___size_buff(&buff);
		if (*buff_size)
			write(fd, buff, *buff_size);
		*buff_size = 0;
	}
}

/*
** Stocke 'data' dans le buff
** si buff est plein alors ecris le buff
** ou si data_len > BUFF_SIZE flush et write data
** retourne la taille write
*/

int
	ft_buff(int fd, void *data, size_t data_len)
{
	void	*buff;
	size_t	*buff_size;
	size_t	nb_write;

	DEBUG;
	nb_write = 0;
	buff = ft_buff___stock(fd);
	buff_size = ft_buff___size_buff(&buff);
	DEBUGNBR(*buff_size);
	DEBUGNBR(data_len);
	DEBUGNBR(fd);
	if (data_len > BUFF_SIZE || data_len + *buff_size > BUFF_SIZE)
	{
		if (*buff_size)
			nb_write = write(fd, buff, *buff_size);
		nb_write = write(fd, data, data_len);
		*buff_size = 0;
	}
	else
	{
		ft_memcpy(buff + *buff_size, data, data_len);
		*buff_size += data_len;
	}
	return (nb_write);
}
