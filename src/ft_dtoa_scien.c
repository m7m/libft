/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dtoa_scien.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:06:19 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:06:21 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_dtoa_scien_end(char *str, int expo)
{
	while (*str)
		++str;
	*str++ = 'e';
	if (expo < 0)
		*str++ = '-';
	else
		*str++ = '+';
	if (expo < 0)
		expo = -expo;
	DEBUGNBR(expo);
	if (expo < 10)
		*str++ = '0';
	if (expo > 99)
		*str++ = expo / 100 % 10 + 48;
	if (expo > 9)
		*str++ = expo / 10 % 10 + 48;
	*str++ = expo % 10 + 48;
	DEBUGSTR(str - 5);
	str = 0;
}

char		*ft_dtoa_scien(double n, int preci)
{
	int		expo;
	int		neg;
	char	*str;

	DEBUGNBR(preci);
	if (ft_isnan(&str, n))
		return (str);
	expo = ft_d_ret_expo(n);
	neg = (n < 0);
	if (expo >= 0)
		n /= ft_pow(10., (double)expo);
	else
		n *= ft_pow(10., (double)-expo);
	str = NULL;
	if (preci >= 0
		&& (str = ft_strnew(sizeof(char) * (preci + 9))))
	{
		if (neg)
			*str = '-';
		ft_dto_str(n, str + neg, 0, preci);
		ft_dtoa_scien_end(str, expo);
	}
	DEBUGSTR(str);
	return (str);
}
