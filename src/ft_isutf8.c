/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isutf8.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 10:09:53 by mmichel           #+#    #+#             */
/*   Updated: 2016/09/22 10:09:53 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "ft_debug.h"

/*
** Retourne la taille du caractère si utf8
** sinon 0
*/

/*
** little/big endian
** if (wc >= 0 && wc <= 0x7F)
** 	wc_size = 1;
** else if ((wc & ~0x1F3F) == (wchar_t)0xC080)
** 	wc_size = 2;
** else if ((wc & ~0xF3F3F) == (wchar_t)0xE08080)
** 	wc_size = 3;
** else if ((wc & ~0x73F3F3F) == (wchar_t)0xF0808080)
** 	wc_size = 4;
*/

size_t
	ft_isutf8(wchar_t wc)
{
	size_t	wc_size;

	DEBUGNBR(wc);
	wc_size = 0;
	if (wc >= 0 && wc <= 0x7F)
		wc_size = 1;
	else if ((wc & ~0x3F1F) == (wchar_t)0x80C0)
		wc_size = 2;
	else if ((wc & ~0x3F3F0F) == (wchar_t)0x8080E0)
		wc_size = 3;
	else if ((wc & ~0x3F3F3F07) == (wchar_t)0x808080F0)
		wc_size = 4;
	DEBUGNBR(wc_size);
	return (wc_size);
}
