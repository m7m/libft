/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 08:26:17 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/05 00:59:21 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Prend en parametre le tableau contenant une liste de mot
** + la longuer de se tableau
** + le nouveau mot a ajouter
** retour: un tableau contenant la liste des mots + le nouveau
*/

static	char	**ft_strsplitpp(char **listmots, size_t len, char *addmot)
{
	char	**tmplistmots;
	size_t	i;

	i = 0;
	if (!(tmplistmots = (char **)ft_memalloc(sizeof(char *) * (len + 2))))
		return (NULL);
	tmplistmots[len] = addmot;
	tmplistmots[len + 1] = 0;
	while (i < len)
	{
		tmplistmots[i] = listmots[i];
		++i;
	}
	free(listmots);
	return (tmplistmots);
}

static	char	**ft_strsplit_add(char **lmotssplit,
							char const *s, int im, int nbmots)
{
	char	*nmotssplit;

	if (!(nmotssplit = ft_strsub((char *)s - im, 0, im)))
		return (NULL);
	lmotssplit = ft_strsplitpp(lmotssplit,
								nbmots,
								nmotssplit);
	return (lmotssplit);
}

char			**ft_strsplit(char const *s, char c)
{
	char	**lmotssplit;
	int		im;
	int		nbmots;

	if (!(lmotssplit = (char **)ft_memalloc(sizeof(char *) * 2)))
		return (NULL);
	*lmotssplit = (char *)s;
	nbmots = 0;
	im = 0;
	while (*s && lmotssplit)
	{
		if (c != *s)
			++im;
		else if (c == *s && im >= 1)
		{
			lmotssplit = ft_strsplit_add(lmotssplit, s, im, nbmots++);
			im = 0;
		}
		++s;
	}
	if (im)
		lmotssplit = ft_strsplit_add(lmotssplit, s, im, nbmots);
	else if (!nbmots && !im)
		*lmotssplit = 0;
	return (lmotssplit);
}
