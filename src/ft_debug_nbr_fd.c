/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_debug_nbr_fd.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/09 21:07:53 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/09 21:07:53 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_debugnbr_fd(int n)
{
	int		fd;

	fd = ft_retfd();
	ft_putnbr_fd(n, fd);
	ft_putchar_fd('\n', fd);
}

void	ft_debugnbrl_fd(long n)
{
	int		fd;
	char	*str;

	fd = ft_retfd();
	if (!(str = ft_ltoa(n)))
		return ;
	ft_putstr_fd(str, fd);
	free(str);
	ft_putchar_fd('\n', fd);
}

void	ft_debugnbrul_fd(t_ul n, char *base)
{
	char	*str;
	int		fd;

	fd = ft_retfd();
	if (!(str = ft_ultoa_base(n, base)))
		return ;
	ft_putstr_fd(str, fd);
	free(str);
	ft_putchar_fd('\n', fd);
}
