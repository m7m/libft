/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 00:25:08 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/23 22:52:09 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Renvoie un pointeur sur la première occurrence
**  du caractère c dans la chaîne s.
*/

/*
** char	*ft_strstr(const char *s1, const char *s2)
** {
** 	int	i;
** 	int	ii;
** 	int	lens2;
** 	int	ms1;
** 	ms1 = 0;
** 	ii = 0;
** 	lens2 = ft_strlen(s2);
** 	i = 0;
** 	while (s1[ii] && s2[i])
** 	{
** 		if (s1[ii] == s2[i])
** 			i++;
** 		else
** 		{
** 			ii = ms1++;
** 			i = 0;
** 		}
** 		ii++;
** 	}
** 	if (i == lens2)
** 		return ((char *)s1 + ii - i);
** 	return (NULL);
** }
*/

char	*ft_strstr(const char *s1, const char *s2)
{
	int i;

	i = 0;
	if (!*s2)
		return ((char *)s1);
	while (*s1)
	{
		while (*s1 == *s2)
		{
			if (!*s1 || !*s2)
				return ((char *)s1 - i);
			i++;
			s1++;
			s2++;
		}
		if (!*s2 && i)
			return ((char *)s1 - i);
		s1 -= i - 1;
		s2 -= i;
		i = 0;
	}
	return (NULL);
}
