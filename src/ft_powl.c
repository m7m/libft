/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_powl.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:52:29 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:52:30 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Fonction de puissance
** x élevé à la puissance y.
*/

long double		ft_powl(long double x, long double y)
{
	long double	p;

	p = 1;
	if (y >= 0)
	{
		++y;
		while (--y)
			p *= x;
	}
	else
	{
		--y;
		while (++y)
			p /= x;
	}
	return (p);
}
