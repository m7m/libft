/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_struct.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/14 01:15:02 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/14 01:15:02 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_STRUCT_H
# define FT_PRINTF_STRUCT_H

/*
** type = (i)int (c)char ...
** size = strlen
** ordre_ap;
** preci = . default = 0
** preci_arg = si non null numero de l'argument pointe
** conv : 1 = # default = 0
** conv_c = symbole convertion
** lang = '
** width;
** sens_width : 1 = defaut 0 = a droite
** *base = ouxX
** remplissage = 0 or ' ', default = ' '
** *add_remplissage = + or ' '
** attr_flags = flags attribut
** minmaj : 1 = 0 = aefg, 1 = AEFG
*/

typedef struct		s_prfarg
{
	union
	{
		char		c;
		int			i;
		short		s;
		long		l;
		t_ll		ll;
		size_t		st;
		ssize_t		sst;
		t_uc		uc;
		t_us		us;
		t_ui		ui;
		t_ul		ul;
		t_ull		ull;
		ssize_t		ust;
		wchar_t		wc;
		wint_t		wi;
		intmax_t	im;
		uintmax_t	uim;
		ptrdiff_t	pd;
		void		*pv;
	}				u;
	int				size;
	int				ordre_ap;
	int				preci;
	int				width;
	int				size_add;
	t_ui			type;
	t_ui			conv_flags;
	t_ui			attr_flags;
	struct s_prfarg	*pc_arg;
	struct s_prfarg	*preci_arg;
	struct s_prfarg	*width_arg;
	struct s_prfarg	*next;
	struct s_prfarg	*nextsort;
	char			*pc;
	char			*base;
	char			add_remplissage[8];
	t_ui			minmaj : 1;
	t_ui			sens_width : 1;
	t_ui			a_sens_width : 14;
	char			conv_c;
	char			remplissage;
}					t_prfarg;

typedef struct		s_prf
{
	t_prfarg		*lstarg[1];
	char			*fmt;
	va_list			ap;
	int				nbap;
	int				nbap_max;
	int				nb_write;
	void			*pnb_write;
	int				fd;
	int				fd_flush : 2;
}					t_prf;

#endif
