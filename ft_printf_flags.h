/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_flags.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/14 01:12:26 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/14 01:12:26 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_FLAGS_H
# define FT_PRINTF_FLAGS_H

/*
** conv_flags:
** (1 << 0) = ' '
** (1 << 1) = -
** (1 << 2) = +
** (1 << 3) = '
** (1 << 4) = I
** (1 << 5) = #
** (1 << 6) = 0
** (1 << 7) = .
** (1 << 8) = $
** (1 << 9) = *
*/

# define FL_NONE B(0)
# define FL_SP B(1)
# define FL_MO B(2)
# define FL_PL B(3)
# define FL_GR B(4)
# define FL_LA B(5)
# define FL_CO B(6)
# define FL_RE B(7)
# define FL_PR B(8)
# define FL_OR B(9)
# define FL_MU B(10)

/*
** Length
*/

/*
** char
** short int
** long int
** long long int
** long double
** long long int
** intmax_t
** size_t
** ptrdiff_t
*/

# define FL_LEN_HH  B(0 + 15)
# define FL_LEN_H   B(1 + 15)
# define FL_LEN_J   B(2 + 15)
# define FL_LEN_Z   B(3 + 15)
# define FL_LEN_T   B(4 + 15)
# define FL_LEN_L   B(5 + 15)
# define FL_LEN_LL  B(6 + 15)
# define FL_LEN_Q   FL_LEN_LL
# define FL_LEN_LLL B(7 + 15)

/*
** Conversion
*/

# define FL_CONV_DI   B(0)
# define FL_CONV_BOUX B(1)
# define FL_CONV_EE   B(2)
# define FL_CONV_FF   B(3)
# define FL_CONV_GG   B(4)
# define FL_CONV_AA   B(5)
# define FL_CONV_C    B(6)
# define FL_CONV_S    B(7)
# define FL_CONV_LC   B(8)
# define FL_CONV_LS   B(9)
# define FL_CONV_P    B(10)
# define FL_CONV_N    B(11)
# define FL_CONV_M    B(12)
# define FL_CONV_POUR B(13)
# define FL_CONV_FD   B(14)

/*
** 1087 = 0b10000111111
** 60 = 0b111100
*/

# define FL_CONV_NUM 1087
# define FL_CONV_DBL 60

/*
** Convert not attribut
*/

# define TMP_CO1 FL_CONV_S | FL_CONV_C
# define TMP_CO2 FL_CONV_N | FL_CONV_DI
# define TMP_CO3 FL_CONV_EE | FL_CONV_FF | FL_CONV_GG
# define FL_CONVNOTATT_CO (TMP_CO1 | TMP_CO2 | TMP_CO3)

#endif
