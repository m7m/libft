/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstiter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/04 01:38:39 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/07 19:40:32 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_lst.h"

void	ft_lstiter(t_list *lst, void (*f)(t_list *elem))
{
	if (lst && lst->next)
		ft_lstiter(lst->next, f);
	if (lst)
		f(lst);
}
