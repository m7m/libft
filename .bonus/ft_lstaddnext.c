/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstaddnext.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 23:43:47 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/05 02:27:44 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Creer l'element en fin de list
*/
#include "libft_lst.h"

void	ft_lstaddnext(t_list *ls, void const *content, size_t content_size)
{
	if (ls->next)
		ft_lstaddnext(ls->next, content, content_size);
	else
		ls->next = ft_lstnew(content, content_size);
}
