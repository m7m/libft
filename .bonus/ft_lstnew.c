/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 18:52:29 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/05 02:31:02 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_lst.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*newmaille;

	newmaille = (t_list *)malloc(sizeof(t_list));
	if (newmaille)
	{
		if (content)
		{
			newmaille->content = malloc(content_size);
			ft_memmove(newmaille->content, content, content_size);
			newmaille->content_size = content_size;
		}
		else
		{
			newmaille->content = NULL;
			newmaille->content_size = 0;
		}
		newmaille->next = NULL;
	}
	return (newmaille);
}
