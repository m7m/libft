/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/05 02:24:46 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/05 02:26:00 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Ajoute l'element en debut de list
*/
#include "libft_lst.h"

void	ft_lstadd(t_list **alst, t_list *new)
{
	new->next = *alst;
	*alst = new;
}
