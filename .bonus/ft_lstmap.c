/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/04 01:53:22 by mmichel           #+#    #+#             */
/*   Updated: 2015/12/05 02:47:56 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_lst.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *maplst;

	if ((maplst = ft_lstnew(lst->content, lst->content_size)))
		maplst = f(maplst);
	if (!maplst)
		return (NULL);
	else if (lst->next)
		maplst->next = ft_lstmap(lst->next, f);
	return (maplst);
}
