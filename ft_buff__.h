/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_buff__.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/13 23:34:19 by mmichel           #+#    #+#             */
/*   Updated: 2016/06/13 23:34:19 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_BUFF___H
# define FT_BUFF___H

void	*ft_buff___static(void);
void	*ft_buff___stock(int fd);
size_t	*ft_buff___size_buff(void **buff);

#endif
