/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_os.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 20:53:52 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 20:53:55 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_OS_H
# define FT_OS_H

# if defined(__linux__) || defined(MODLINUX)
#  define LINUX 1
#  define MACH 0
#  define MODLINUX 1
# elif defined(__APPLE__) || defined(__MACH__)
#  define LINUX 0
#  define MACH 1
# else
#  define LINUX 1
#  define MACH 0
#  define MODLINUX 1
# endif

#endif
