/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prf_status.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmichel <mmichel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 21:34:17 by mmichel           #+#    #+#             */
/*   Updated: 2016/05/17 21:46:17 by mmichel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_STATUS_H
# define FT_PRINTF_STATUS_H

# include <string.h>

typedef struct	s_status
{
	char	*msg;
	int		status;
	int		pass;
}				t_status;

#endif
